<?php

/**
 * @file
 * Main file for all hook preprocess.
 */

use Drupal\wtotem\lib\Common\Cache;
use Drupal\wtotem\lib\Common\Helper;
use Drupal\wtotem\lib\Common\API as WtotemAPI;

/**
 * Implements template_preprocess_HOOK() for wtotem_login_form.
 */
function template_preprocess_wtotem_login_form(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
  $variables['notifications'] = Helper::getNotifications();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_agents_installation.
 */
function template_preprocess_wtotem_agents_installation(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_header.
 */
function template_preprocess_wtotem_header(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_agents.
 */
function template_preprocess_wtotem_agents(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_notifications.
 */
function template_preprocess_wtotem_notifications(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_scanning.
 */
function template_preprocess_wtotem_scanning(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_monitoring.
 */
function template_preprocess_wtotem_monitoring(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_firewall.
 */
function template_preprocess_wtotem_firewall(&$variables) {
	$variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_firewall_header.
 */
function template_preprocess_wtotem_firewall_header(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_footer.
 */
function template_preprocess_wtotem_footer(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_antivirus_logs.
 */
function template_preprocess_wtotem_antivirus_logs(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_antivirus_logs.
 */
function template_preprocess_wtotem_antivirus(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_antivirus_filter_form.
 */
function template_preprocess_wtotem_antivirus_filter_form(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_ignore_ports.
 */
function template_preprocess_wtotem_ignore_ports(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_open_ports.
 */
function template_preprocess_wtotem_open_ports(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_allow_deny_list.
 */
function template_preprocess_wtotem_allow_deny_list(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_allow_url_list.
 */
function template_preprocess_wtotem_allow_url_list(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_country_blocking_modal.
 */
function template_preprocess_wtotem_country_blocking_modal(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_authorization_needed.
 */
function template_preprocess_wtotem_authorization_needed(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_waf_filter_form.
 */
function template_preprocess_wtotem_waf_filter_form(&$variables) {
  $variables['#attached']['library'][] = 'wtotem/wtotem.firewall';
}

/**
 * Implements template_preprocess_HOOK() for wtotem_reports_form.
 */
function template_preprocess_wtotem_reports_form(&$variables) {
  $variables['#attached']['library'][] = 'wtotem/wtotem.reports';
}

/**
 * Implements template_preprocess_HOOK() for wtotem_open_paths.
 */
function template_preprocess_wtotem_open_paths(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
}

/**
 * Implements template_preprocess_HOOK() for wtotem_open_paths_page.
 */
function template_preprocess_wtotem_open_paths_page(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();
  $variables['#attached']['library'][] = 'wtotem.main_css';
}

/**
 * Implements template_preprocess_HOOK() for wtotem_score.
 */
function template_preprocess_wtotem_score(&$variables) {
  if ($variables['tested_on']) {
    $tested_on = strtotime($variables['tested_on']);

    $date_formatter = \Drupal::service('date.formatter');
    $variables['tested_on'] = $date_formatter->format($tested_on, 'custom', 'Y-m-d, H:i');
  }
}

/**
 * Implements template_preprocess_HOOK() for wtotem_allow_deny_form.
 */
function template_preprocess_wtotem_allow_deny_form(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();

  $host = WtotemAPI::siteInfo();

  if($cacheData = Cache::getdata('getIpLists', $host['id'])){
    $ip_list = $cacheData['data'];
  } else {
    $ip_list = WtotemAPI::getIpLists($host['id']);
    Cache::setData(['getIpLists' => $ip_list], $host['id']);
  }

  if($cacheData = Cache::getdata('getAllowUrlList', $host['id'])){
    $url_list = $cacheData['data'];
  } else {
    $url_list = WtotemAPI::getAllowUrlList($host['id']);
    Cache::setData(['getAllowUrlList' => $url_list], $host['id']);
  }

  $variables['deny_list'] = Helper::getIpList($ip_list['blackList'], 'ip_deny');
  $variables['allow_list'] = Helper::getIpList($ip_list['whiteList'], 'ip_allow');
  $variables['url_list'] = Helper::getUrlAllowList($url_list);
}

/**
 * Implements template_preprocess_HOOK() for wtotem_ports_form.
 */
function template_preprocess_wtotem_ports_form(&$variables) {
  $variables['images_path'] = wtotem_get_images_path();

  $host = WtotemAPI::siteInfo();

  if($cacheData = Cache::getdata('getAllPortsList', $host['id'])){
    $variables['ports'] = $cacheData['data'];
  } else {
    $variables['ports'] = WtotemAPI::getAllPortsList($host['id']);
    Cache::setData(['getAllPortsList' => $variables['ports']], $host['id']);
  }
}

/**
 * Implements template_preprocess_HOOK() for wtotem_settings_form.
 */
function template_preprocess_wtotem_settings_form(&$variables) {

  $host = WtotemAPI::siteInfo();

  if($cacheData = Cache::getdata('getAgentsStatuses', $host['id'])){
    $agents_statuses = $cacheData['data'];
  } else {
    $agents_statuses = WtotemAPI::getAgentsStatuses($host['id']);
    Cache::setData(['getAgentsStatuses' => $agents_statuses], $host['id']);
  }

  $variables['av_status'] = Helper::getStatusData($agents_statuses['av']['status']);
  $variables['waf_status'] = Helper::getStatusData($agents_statuses['waf']['status']);

  if($cacheData = Cache::getdata('getIpLists', $host['id'])){
    $ip_list = $cacheData['data'];
  } else {
    $ip_list = WtotemAPI::getIpLists($host['id']);
    Cache::setData(['getIpLists' => $ip_list], $host['id']);
  }

  if($cacheData = Cache::getdata('getAllowUrlList', $host['id'])){
    $url_list = $cacheData['data'];
  } else {
    $url_list = WtotemAPI::getAllowUrlList($host['id']);
    Cache::setData(['getAllowUrlList' => $url_list], $host['id']);
  }

  if($cacheData = Cache::getdata('getBlockedCountries', $host['id'])){
    $waf_data = $cacheData['data'];
  } else {
    $waf_data = WtotemAPI::getBlockedCountries($host['id']);
    Cache::setData(['getBlockedCountries' => $waf_data], $host['id']);
  }

  $variables['blocked_countries_count'] = Helper::getBlockedCountriesCount($waf_data['blockedCountries']);
  $variables['mock_attacks'] = Helper::getTreeMostAttacksData($waf_data['map']);
  $variables['deny_list'] = Helper::getIpList($ip_list['blackList'], 'ip_deny');
  $variables['allow_list'] = Helper::getIpList($ip_list['whiteList'], 'ip_allow');
  $variables['url_list'] = Helper::getUrlAllowList($url_list);

  // Header variables.
  $variables['theme_mode'] = Helper::getThemeMode();
  $variables['notifications'] = Helper::getNotifications();
  $variables['is_active'] = ['settings' => 'wtotem_nav__link_active'];

	$variables['images_path'] = wtotem_get_images_path();
	$variables['current_year'] = date('Y');

}

/**
 * Get the path to the image folder.
 *
 * @return string
 *   Returns the path to the image folder.
 */
function wtotem_get_images_path() {
  return WTOTEM_MODULE_PATH . '/img/';
}
