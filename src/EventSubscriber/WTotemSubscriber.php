<?php

namespace Drupal\wtotem\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Session\AccountInterface;

use Drupal\wtotem\lib\Common\App;

/**
 * Class subscriber.
 */
class WTotemSubscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The constructor method.
   *
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   */
  public function __construct(AccountInterface $currentUser) {
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['includeWaf', 300];
    return $events;
  }

  /**
   * Include waf file before loading drupal.
   */
  public function includeWaf() {
    $waf_file = App::getOption('waf_file');
    $waf_include_file = DRUPAL_ROOT . '/_include_' . $waf_file;

    if (!$this->hasPermissionToSkipWafCheck()) {
      if (file_exists($waf_include_file)) {
        include_once $waf_include_file;
      }
    }
  }

  /**
   * Check permission.
   *
   * @return bool
   *   Returns whether the user has permission to skip the firewall check.
   */
  private function hasPermissionToSkipWafCheck() {
    if ($this->currentUser->hasPermission('skip wtotem waf check')) {
      return TRUE;
    }
    return FALSE;
  }

}
