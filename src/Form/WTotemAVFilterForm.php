<?php

namespace Drupal\wtotem\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\wtotem\lib\Common\API as WtotemAPI;
use Drupal\wtotem\lib\Common\App;
use Drupal\wtotem\lib\Common\Helper;

/**
 * Form with modal window.
 */
class WTotemAVFilterForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wtotem_antivirus_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['file_status'] = [
      '#type' => 'select',
      '#options' => [
        'new' => $this->t('New files'),
        'scanned' => $this->t('Scanned files'),
        'modified' => $this->t('Changed files'),
        'deleted' => $this->t('Deleted files'),
        'infected' => $this->t('Infected files'),
      ],
      '#default_value' => 'new',
      '#attributes' => [
        'class' => ['wtotem_state__select'],
      ],
      '#ajax' => [
        'callback' => '::ajaxFilter',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
      '#theme_wrappers' => [],
    ];

    $form['permission'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Permissions changed'),
      '#attributes' => [
        'class' => ['wt-checkbox__input'],
      ],
      '#label_attributes' => [
        'class' => ['wt-checkbox__label'],
      ],
      '#title_display' => 'after',
      '#ajax' => [
        'callback' => '::ajaxFilter',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) { }

  /**
   * Add data filter.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxFilter(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();

    $file_status = $form_state->getValue('file_status');
    $permission = $form_state->getValue('permission');
    $host = WtotemAPI::siteInfo();

    App::setSessionOptions([
      'antivirus_permissions' => $permission,
      'antivirus_event' => $file_status,
    ]);

    $params = [
      'host_id' => $host['id'],
      'limit' => 10,
      'days' => 365,
      'cursor' => NULL,
      'event' => $file_status,
      'permissions' => $permission,
    ];

    $data = WtotemAPI::getAntivirus($params);
    $has_next_page = $data['log']['pageInfo']['hasNextPage'];

    App::setSessionOptions([
      'antivirus_cursor' => $data['log']['pageInfo']['endCursor'],
    ]);

    $build[] = [
      "#logs" => Helper::getAntivirusLogs($data['log']['edges']),
      '#theme' => 'wtotem_antivirus_logs',
    ];

    // Commands Ajax.
    if (!$has_next_page) {
      $ajax_response->addCommand(new InvokeCommand('#av_load_more', 'remove'));
    }
    else {
      $link = '<a href="/wtotem/lazy_load/antivirus" class="use-ajax wtotem_more_btn" id="av_load_more">' . $this->t('Show more') . '</a>';
      $ajax_response->addCommand(new HtmlCommand('.wtotem_more_btn_wrap', $link, $settings = NULL));
    }

    $ajax_response->addCommand(new HtmlCommand('#av_logs_wrap', $build));

    return $ajax_response;
  }

}
