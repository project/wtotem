<?php

namespace Drupal\wtotem\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\wtotem\lib\Common\API as WtotemAPI;
use Drupal\wtotem\lib\Common\App;
use Drupal\wtotem\lib\Common\Cache;
use Drupal\wtotem\lib\Common\Helper;

/**
 * Form with modal window.
 */
class WTotemPortsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wtotem_ports_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['messages'] = [
      '#markup' => '<div id="wtotem_ports_form-messages"></div>',
      '#weight' => -100,
    ];

    $form['port'] = [
      '#type' => 'number',
      '#attributes' => [
        'placeholder' => $this->t('Type port number'),
      ],
      '#theme_wrappers' => [],
    ];

    $form['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Send'),
      '#attributes' => [
        'class' => ['button'],
      ],
      '#ajax' => [
        'callback' => '::ajaxSubmitCallback',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) { }

  /**
   * {@inheritdoc}
   */
  public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();

    $host = WtotemAPI::siteInfo();
    $response = WtotemAPI::addIgnorePort($host['id'], $form_state->getValue('port'));

    if ($response['errors']) {
      $massage = '<div class="message error_message">' . $this->t('Failed') . '</div>';
    }
    else {
      $massage = '<div class="message success_message">' . $this->t('Success') . '</div>';
    }

    $ports = WtotemAPI::getAllPortsList($host['id']);
    Cache::setData(['getAllPortsList' => $ports], $host['id']);

    $open_ports[] = [
      "#ports" => Helper::getOpenPortsData($ports['TCPResults']),
      '#theme' => 'wtotem_open_ports',
    ];

    $open_ports_few[] = [
      "#more" => true,
      "#ports" => $ports['TCPResults'] ? Helper::getOpenPortsData(array_slice($ports['TCPResults'], 0, 3)) : [],
      '#theme' => 'wtotem_open_ports',
    ];

    $ignore_ports[] = [
      "#ports" => $ports,
      '#theme' => 'wtotem_ignore_ports',
    ];

    $status = Helper::getStatusData($ports['status']);

    // Commands Ajax.
    $ajax_response->addCommand(new HtmlCommand('#open_ports_wrap', $open_ports_few ?? ''));
    $ajax_response->addCommand(new HtmlCommand('#port_result_list', $open_ports ?? ''));
    $ajax_response->addCommand(new HtmlCommand('#ignore_ports_wrap', $ignore_ports));
    $ajax_response->addCommand(new HtmlCommand('#port_status', $status['text']));
    $ajax_response->addCommand(new InvokeCommand('#port_status', 'addClass', [$status['class']]));
    $ajax_response->addCommand(new HtmlCommand('#port_last_check', Helper::dateFormatter($ports['lastTest']['time'])));

    App::setNotification('success', $massage);
    //$ajax_response->addCommand(new HtmlCommand('#wtotem_ports_form-messages', $massage));
    self::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Notification output.
   *
   * @param mixed $ajax_response
   *   AjaxResponse.
   */
  public static function notifications(&$ajax_response) {

    $notifications = Helper::getNotifications();

    $build[] = [
      "#notifications" => $notifications,
      '#attached' => [
        'library' => [
          'wtotem/wtotem.notifications',
        ],
      ],
      '#theme' => 'wtotem_notifications',
    ];

    // Commands Ajax.
    $ajax_response->addCommand(new AppendCommand('#wtotem_notifications', $build));
  }

}
