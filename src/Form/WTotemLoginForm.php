<?php

namespace Drupal\wtotem\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\wtotem\lib\Common\AgentManager;
use Drupal\wtotem\lib\Common\API as WtotemAPI;
use Drupal\wtotem\lib\Common\App;

/**
 * Implements a form to collect security check configuration.
 */
class WTotemLoginForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wtotem_login_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $api_key = App::getOption('api_key');
    $activated = App::activated();

    // Checks if the site has been added to the Wtotem Cabinet.
    if ($api_key) {
      $host = WtotemAPI::siteInfo();
    }

    // Install Agent Manager if it was not previously installed.
    $am_installed = AgentManager::checkInstalledService('am');
    if ($api_key && $host['id']) {
      if (!$am_installed['option_status'] || !$am_installed['file_status']) {
        $am_was_installed = AgentManager::amInstall();
        if (!$am_was_installed) {
          App::setOptions(['am_installed' => FALSE]);
        }
      }
    }

    if ($activated) {
      $activated_status = '<span class="wtotem_alert__title_green">' . $this->t('Module is activated') . '</span>';
    }
    else {
      $activated_status = '<span class="wtotem_alert__title_red">' . $this->t('Module is not activated') . '</span>';
    }

    $form['activated_status'] = [
      '#markup' => $activated_status,
    ];

    if ($api_key) {
      $form['current_api_key'] = [
        '#markup' => $this->t('Your current API key is :') . '<br>' . strstr($api_key, '-', TRUE) . '-************',
      ];
    }

    $form['api_key'] = [
      '#maxlength' => 100,
      '#placeholder' => $this->t('API Key Activation'),
      '#required' => TRUE,
      '#type' => 'textfield',
      '#attributes' => [
        'class' => ['wtotem_modal__api-key'],
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $activated ? $this->t('UPDATE') : $this->t('ACTIVATE'),
      '#attributes' => [
        'class' => ['wtotem_modal__btn'],
      ],
    ];

    $form['#attached']['library'][] = 'wtotem/wtotem.main_css';
    $form['#attached']['library'][] = 'wtotem/wtotem.main_js';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $api_key = $form_state->getValue('api_key');
    $result = WtotemAPI::auth($api_key);

    if ($result === 'success') {
      App::setNotification('success', 'Your plugin is successfully activated');
    }
  }

}
