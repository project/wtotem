<?php

namespace Drupal\wtotem\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\wtotem\lib\Common\API as WtotemAPI;
use Drupal\wtotem\lib\Common\App;
use Drupal\wtotem\lib\Common\Helper;

/**
 * Form with modal window.
 */
class WTotemWafFilterForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wtotem_waf_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['from'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'class' => ['wtotem_calendar', 'flatpickr', 'flatpickr-input'],
        'placeholder' => $this->t('Select Date'),
        'readonly' => TRUE,
      ],
	    '#attached' => [
			    'library' => [
					    'wtotem/wtotem.dashboard',
			    ],
	    ],
      '#ajax' => [
        'callback' => '::dateFilter',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
      '#theme_wrappers' => [],

    ];

    $form['to'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'class' => ['wtotem_calendar'],
        'placeholder' => $this->t('Select Date'),
        'readonly' => TRUE,
      ],
		    '#attached' => [
				    'library' => [
						    'wtotem/wtotem.dashboard',
				    ],
		    ],
      '#ajax' => [
        'callback' => '::dateFilter',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
      '#theme_wrappers' => [],

    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) { }

  /**
   * Add date filter.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function dateFilter(array $form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();

    $period = explode(" to ", $form_state->getValue('from'));
    App::setSessionOptions(['firewall_period' => $period]);

    $host = WtotemAPI::siteInfo();

    // Firewall logs.
    $data = WtotemAPI::getFirewall($host['id'], 10, NULL, $period);
    $firewall = $data['firewall'];

    $waf_logs[] = [
      "#logs" => Helper::wafLogs($firewall['logs']['edges']),
      '#theme' => 'wtotem_firewall_logs',
    ];

    // Firewall chart.
    $data = WtotemAPI::getFirewallChart($host['id'], $period);
    $chart = Helper::generateWafChart($data['chart']);

    $waf_chart[] = [
      "#days" => $chart['days'],
      "#chart" => $chart['chart'],
      '#theme' => 'wtotem_firewall_chart',
    ];

    // Firewall stats.
    $waf_stats[] = [
      "#is_waf_training" => Helper::isWafTraining($data['agentManager']['createdAt']),
      "#all_attacks" => $chart['count_attacks'],
      "#blocking" => $chart['count_blocks'],
      "#not_blocking" => $chart['count_attacks'] - $chart['count_blocks'],
      "#most_attacks" => Helper::getMostAttacksData($firewall['map']),
      '#theme' => 'wtotem_firewall_stats',
    ];

    App::setSessionOptions([
      'firewall_cursor' => $firewall['logs']['pageInfo']['endCursor'],
    ]);

    $has_next_page = $firewall['logs']['pageInfo']['hasNextPage'];

    if (!$has_next_page) {
      $ajax_response->addCommand(new InvokeCommand('#waf_load_more', 'remove'));
    }
    else {
      $link = '<a href="/wtotem/lazy_load/firewall" class="use-ajax wtotem_more_btn" id="waf_load_more">' . $this->t('Show more') . '</a>';
      $ajax_response->addCommand(new HtmlCommand('.wtotem_more_btn_wrap', $link, $settings = NULL));
    }

    $ajax_response->addCommand(new HtmlCommand('#waf_chart_wrap', $waf_chart, $settings = NULL));
    $ajax_response->addCommand(new HtmlCommand('#waf_logs_wrap', $waf_logs, $settings = NULL));
    $ajax_response->addCommand(new ReplaceCommand('#firewall_stats', $waf_stats, $settings = NULL));

    return $ajax_response;
  }

}
