<?php

namespace Drupal\wtotem\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\wtotem\lib\Common\API as WtotemAPI;
use Drupal\wtotem\lib\Common\Cache;
use Drupal\wtotem\lib\Common\Helper;

/**
 * Form with modal window.
 */
class WTotemReportsForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'wtotem_reports_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['messages'] = [
      '#markup' => '<div id="wtotem_reports_form-messages"></div>',
      '#weight' => -100,
    ];

    $form['report_date_from'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'class' => [
          'wtotem_calendar',
          'flatpickr',
          'flatpickr-input',
        ],
        'placeholder' => $this->t('Select Date'),
        'readonly' => TRUE,
      ],
      '#theme_wrappers' => [],

    ];

    $form['report_date_to'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'class' => ['wtotem_calendar'],
        'placeholder' => $this->t('Select Date'),
        'readonly' => TRUE,
      ],
      '#theme_wrappers' => [],

    ];

    $form['module_availability'] = [
      '#type' => 'checkbox',
      '#title' => '.',
      '#default_value' => 1,
      '#attributes' => [
        'class' => ['wtotem_reports-settings__module-checkbox'],
      ],
      '#label_attributes' => [
        'class' => ['wtotem_reports-settings__module-label'],
      ],
      '#title_display' => 'after',
    ];

    $form['module_deface'] = [
      '#type' => 'checkbox',
      '#title' => '.',
      '#default_value' => 1,
      '#attributes' => [
        'class' => ['wtotem_reports-settings__module-checkbox'],
      ],
      '#label_attributes' => [
        'class' => ['wtotem_reports-settings__module-label'],
      ],
      '#title_display' => 'after',
    ];

    $form['module_ports'] = [
      '#type' => 'checkbox',
      '#title' => '.',
      '#default_value' => 1,
      '#attributes' => [
        'class' => ['wtotem_reports-settings__module-checkbox'],
      ],
      '#label_attributes' => [
        'class' => ['wtotem_reports-settings__module-label'],
      ],
      '#title_display' => 'after',
    ];

    $form['module_reputation'] = [
      '#type' => 'checkbox',
      '#title' => '.',
      '#default_value' => 1,
      '#attributes' => [
        'class' => ['wtotem_reports-settings__module-checkbox'],
      ],
      '#label_attributes' => [
        'class' => ['wtotem_reports-settings__module-label'],
      ],
      '#title_display' => 'after',
    ];

    $form['module_scoring'] = [
      '#type' => 'checkbox',
      '#title' => '.',
      '#default_value' => 1,
      '#attributes' => [
        'class' => ['wtotem_reports-settings__module-checkbox'],
      ],
      '#label_attributes' => [
        'class' => ['wtotem_reports-settings__module-label'],
      ],
      '#title_display' => 'after',
    ];

    $form['module_antivirus'] = [
      '#type' => 'checkbox',
      '#title' => '.',
      '#default_value' => 1,
      '#attributes' => [
        'class' => ['wtotem_reports-settings__module-checkbox'],
      ],
      '#label_attributes' => [
        'class' => ['wtotem_reports-settings__module-label'],
      ],
      '#title_display' => 'after',
    ];

    $form['module_firewall'] = [
      '#type' => 'checkbox',
      '#title' => '.',
      '#default_value' => 1,
      '#attributes' => [
        'class' => ['wtotem_reports-settings__module-checkbox'],
      ],
      '#label_attributes' => [
        'class' => ['wtotem_reports-settings__module-label'],
      ],
      '#title_display' => 'after',
    ];

    $form['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Generate new report'),
      '#attributes' => [
        'class' => [
          'wtotem_reports__btn',
          'wtotem_reports-settings__btn',
          'button',
        ],
      ],
      '#ajax' => [
        'callback' => '::ajaxSubmitCallback',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) { }

  /**
   * {@inheritdoc}
   */
  public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();

    $period = explode(" to ", $form_state->getValue('report_date_from'));

    $modules = [
      'wa' => $form_state->getValue('module_availability') ? 'true' : 'false',
      'dc' => $form_state->getValue('module_deface') ? 'true' : 'false',
      'ps' => $form_state->getValue('module_ports') ? 'true' : 'false',
      'rc' => $form_state->getValue('module_reputation') ? 'true' : 'false',
      'sc' => $form_state->getValue('module_scoring') ? 'true' : 'false',
      'av' => $form_state->getValue('module_antivirus') ? 'true' : 'false',
      'waf' => $form_state->getValue('module_firewall') ? 'true' : 'false',
    ];

    $host = WtotemAPI::siteInfo();

    $response = WtotemAPI::generateReport($host['id'], $period, $modules);

    if (!$response) {
      $massage = '<div class="message error_message">' . $this->t('Report generation error') . '</div>';
    }
    else {
      $massage = '<div class="message success_message">' . $this->t('The report was successfully generated') . '</div>';

      $data = WtotemAPI::getAllReports($host['id']);

	    Cache::setData(['getAllReports' => $data], $host['id']);

      // Reports.
      $build[] = [
        "#reports" => Helper::getReports($data['edges']),
        "#has_next_page" => $data['pageInfo']['hasNextPage'],
        '#theme' => 'wtotem_reports_list',
      ];

      // Reports mobile.
      $build_mobile[] = [
        "#reports" => Helper::getReports($data['edges']),
        "#has_next_page" => $data['pageInfo']['hasNextPage'],
        '#theme' => 'wtotem_reports_list_mobile',
      ];

      $ajax_response->addCommand(new CssCommand('.wtotem_reports-list', ['display' => 'block']));
      $ajax_response->addCommand(new CssCommand('.wtotem_reports-no-data', ['display' => 'none']));
      $ajax_response->addCommand(new HtmlCommand('#reports_logs_wrap', $build));
      $ajax_response->addCommand(new HtmlCommand('#reports_m_logs_wrap', $build_mobile));
      $ajax_response->addCommand(new RedirectCommand($response));
    }

    $ajax_response->addCommand(new HtmlCommand('#wtotem_reports_form-messages', $massage));

    return $ajax_response;
  }

}
