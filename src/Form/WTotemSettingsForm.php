<?php

namespace Drupal\wtotem\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\wtotem\Controller\WTotemAjaxController;
use Drupal\wtotem\lib\Common\API as WtotemAPI;
use Drupal\wtotem\lib\Common\App;
use Drupal\wtotem\lib\Common\Helper;
use Drupal\wtotem\lib\Common\Cache;

/**
 * Form with modal window.
 */
class WTotemSettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wtotem_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $host = WtotemAPI::siteInfo();

    $data = WtotemAPI::getConfigs($host['id']);

    $configs = Helper::arrayMapIndex($data, 'service');

    App::setSessionOptions(['configs' => $configs]);

    $form['#attached']['library'] = [
      'wtotem/wtotem.settings',
      'wtotem/wtotem.header',
    ];

    // Modules settings.
    $form['module_waf'] = [
      '#type' => 'checkbox',
      '#title' => '<span class="onoff__circle"></span>',
      '#default_value' => $configs['waf']['isActive'],
      '#attributes' => [
        'class' => ['onoff__input'],
      ],
      '#label_attributes' => [
        'class' => ['onoff__label'],
      ],
      '#title_display' => 'after',
      '#ajax' => [
        'callback' => '::ajaxToggleModuleWaf',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['module_av'] = [
      '#type' => 'checkbox',
      '#title' => '<span class="onoff__circle"></span>',
      '#default_value' => $configs['av']['isActive'],
      '#attributes' => [
        'class' => ['onoff__input'],
      ],
      '#label_attributes' => [
        'class' => ['onoff__label'],
      ],
      '#title_display' => 'after',
      '#ajax' => [
        'callback' => '::ajaxToggleModuleAv',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['module_ss'] = [
      '#type' => 'checkbox',
      '#title' => '<span class="onoff__circle"></span>',
      '#default_value' => $configs['ss']['isActive'],
      '#attributes' => [
        'class' => ['onoff__input'],
      ],
      '#label_attributes' => [
        'class' => ['onoff__label'],
      ],
      '#title_display' => 'after',
      '#ajax' => [
        'callback' => '::ajaxToggleModuleSs',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['module_wa'] = [
      '#type' => 'checkbox',
      '#title' => '<span class="onoff__circle"></span>',
      '#default_value' => $configs['wa']['isActive'],
      '#attributes' => [
        'class' => ['onoff__input'],
      ],
      '#label_attributes' => [
        'class' => ['onoff__label'],
      ],
      '#title_display' => 'after',
      '#ajax' => [
        'callback' => '::ajaxToggleModuleWa',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['module_dc'] = [
      '#type' => 'checkbox',
      '#title' => '<span class="onoff__circle"></span>',
      '#default_value' => $configs['dc']['isActive'],
      '#attributes' => [
        'class' => ['onoff__input'],
      ],
      '#label_attributes' => [
        'class' => ['onoff__label'],
      ],
      '#title_display' => 'after',
      '#ajax' => [
        'callback' => '::ajaxToggleModuleDC',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['module_ps'] = [
      '#type' => 'checkbox',
      '#title' => '<span class="onoff__circle"></span>',
      '#default_value' => $configs['ps']['isActive'],
      '#attributes' => [
        'class' => ['onoff__input'],
      ],
      '#label_attributes' => [
        'class' => ['onoff__label'],
      ],
      '#title_display' => 'after',
      '#ajax' => [
        'callback' => '::ajaxToggleModulePs',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['module_rc'] = [
      '#type' => 'checkbox',
      '#title' => '<span class="onoff__circle"></span>',
      '#default_value' => $configs['rc']['isActive'],
      '#attributes' => [
        'class' => ['onoff__input'],
      ],
      '#label_attributes' => [
        'class' => ['onoff__label'],
      ],
      '#title_display' => 'after',
      '#ajax' => [
        'callback' => '::ajaxToggleModuleRc',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['module_cms'] = [
      '#type' => 'checkbox',
      '#title' => '<span class="onoff__circle"></span>',
      '#default_value' => $configs['cms']['isActive'],
      '#attributes' => [
        'class' => ['onoff__input'],
      ],
      '#label_attributes' => [
        'class' => ['onoff__label'],
      ],
      '#title_display' => 'after',
      '#ajax' => [
        'callback' => '::ajaxToggleModuleCms',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    // Notification settings.
    $form['notification_wa'] = [
      '#type' => 'checkbox',
      '#title' => '<span class="onoff__circle"></span>',
      '#default_value' => $configs['wa']['notifications'],
      '#attributes' => [
        'class' => ['onoff__input'],
      ],
      '#label_attributes' => [
        'class' => ['onoff__label'],
      ],
      '#title_display' => 'after',
      '#ajax' => [
        'callback' => '::ajaxToggleNotificationWa',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['notification_av'] = [
      '#type' => 'checkbox',
      '#title' => '<span class="onoff__circle"></span>',
      '#default_value' => $configs['av']['notifications'],
      '#attributes' => [
        'class' => ['onoff__input'],
      ],
      '#label_attributes' => [
        'class' => ['onoff__label'],
      ],
      '#title_display' => 'after',
      '#ajax' => [
        'callback' => '::ajaxToggleNotificationAv',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['notification_dc'] = [
      '#type' => 'checkbox',
      '#title' => '<span class="onoff__circle"></span>',
      '#default_value' => $configs['dc']['notifications'],
      '#attributes' => [
        'class' => ['onoff__input'],
      ],
      '#label_attributes' => [
        'class' => ['onoff__label'],
      ],
      '#title_display' => 'after',
      '#ajax' => [
        'callback' => '::ajaxToggleNotificationDc',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['notification_rc'] = [
      '#type' => 'checkbox',
      '#title' => '<span class="onoff__circle"></span>',
      '#default_value' => $configs['rc']['notifications'],
      '#attributes' => [
        'class' => ['onoff__input'],
      ],
      '#label_attributes' => [
        'class' => ['onoff__label'],
      ],
      '#title_display' => 'after',
      '#ajax' => [
        'callback' => '::ajaxToggleNotificationRc',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['notification_ps'] = [
      '#type' => 'checkbox',
      '#title' => '<span class="onoff__circle"></span>',
      '#default_value' => $configs['ps']['notifications'],
      '#attributes' => [
        'class' => ['onoff__input'],
      ],
      '#label_attributes' => [
        'class' => ['onoff__label'],
      ],
      '#title_display' => 'after',
      '#ajax' => [
        'callback' => '::ajaxToggleNotificationPs',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['notification_waf'] = [
      '#type' => 'checkbox',
      '#title' => '<span class="onoff__circle"></span>',
      '#default_value' => $configs['waf']['notifications'],
      '#attributes' => [
        'class' => ['onoff__input'],
      ],
      '#label_attributes' => [
        'class' => ['onoff__label'],
      ],
      '#title_display' => 'after',
      '#ajax' => [
        'callback' => '::ajaxToggleNotificationWaf',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    // Allow/Deny IP and url lists.
    $form['allow_ip'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'placeholder' => $this->t('Type IPv4 or IPv6 address or a mask (104.122.249.38 or 104.122.*.*)'),
        'class' => ['firewall-configuration__ip-adress'],
        'autocomplete' => 'off',
      ],
      '#theme_wrappers' => [],
    ];

    $form['allow_ip_submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Add IP') . ' ',
      '#attributes' => [
        'class' => ['wtotem_control__btn', 'wtotem_btn_wc'],
      ],
      '#ajax' => [
        'callback' => '::ajaxSubmitAllowIp',
      ],
    ];

    $form['deny_ip'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'placeholder' => $this->t('Type IPv4 or IPv6 address or a mask (104.122.249.38 or 104.122.*.*)'),
        'class' => ['firewall-configuration__ip-adress'],
        'autocomplete' => 'off',
      ],
      '#theme_wrappers' => [],
    ];

    $form['deny_ip_submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Add IP'),
      '#attributes' => [
        'class' => ['wtotem_control__btn', 'wtotem_btn_wc'],
      ],
      '#ajax' => [
        'callback' => '::ajaxSubmitDenyIp',
      ],
    ];

    $form['allow_url'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'placeholder' => $this->t('path/'),
        'class' => ['firewall-configuration__ip-adress'],
        'autocomplete' => 'off',
      ],
      '#theme_wrappers' => [],
    ];

    $form['allow_url_submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Add URL'),
      '#attributes' => [
        'class' => ['wtotem_control__btn', 'wtotem_btn_wc'],
      ],
      '#ajax' => [
        'callback' => '::ajaxSubmitAllowUrl',
      ],
    ];

    // Firewall settings.
    if($cacheData = Cache::getdata('getFirewall', $host['id'])){
      $data = $cacheData['data'];
    } else {
      $data = WtotemAPI::getFirewall($host['id']);
      Cache::setData(['getFirewall' => $data], $host['id']);
    }
    $settings = $data['firewall']['settings'];

    $form['gdn'] = [
      '#type' => 'checkbox',
      '#default_value' => (isset($settings['gdn'])) ? $settings['gdn'] : TRUE,
      '#attributes' => [
        'class' => ['onoff__input', 'waf_settings_toggle', json_encode($settings)],
      ],
      '#theme_wrappers' => [],
    ];

    $form['dos'] = [
      '#type' => 'checkbox',
      '#default_value' => (isset($settings['dosProtection'])) ? $settings['dosProtection'] : TRUE,
      '#attributes' => [
        'class' => ['onoff__input', 'waf_settings_toggle'],
      ],
      '#theme_wrappers' => [],
    ];

    $form['login_attempt'] = [
      '#type' => 'checkbox',
      '#default_value' => (isset($settings['loginAttemptsProtection'])) ? $settings['loginAttemptsProtection'] : TRUE,
      '#attributes' => [
        'class' => ['onoff__input', 'waf_settings_toggle'],
      ],
      '#theme_wrappers' => [],
    ];

    $form['dos_limit'] = [
      '#type' => 'number',
      '#default_value' => $settings['dosLimit'] ?: 1000,
      '#required' => TRUE,
      '#min' => 0,
      '#attributes' => [
        'class' => ['wtotem-waf-settings-popup__input'],
      ],
      '#states' => [
        'visible' => [
          ':input[name="dos"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['login_attempt_limit'] = [
      '#type' => 'number',
      '#default_value' => $settings['loginAttemptsLimit'] ?: 20,
      '#required' => TRUE,
      '#min' => 0,
      '#attributes' => [
        'class' => ['wtotem-waf-settings-popup__input'],
      ],
      '#states' => [
        'visible' => [
          ':input[name="login_attempt"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['waf_submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Save settings'),
      '#attributes' => [
        'class' => ['wtotem_control__btn'],
      ],
      '#ajax' => [
        'callback' => '::ajaxSubmitCallback',
      ],
    ];

    // IP addresses multi adding.
    $form['list'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'wtotem-ip-list-type',
      ],
      '#theme_wrappers' => [],
    ];

    $form['ips'] = [
      '#type' => 'textarea',
      '#attributes' => [
        'placeholder' => $this->t('Example: 104.122.249.38, 104.122.249.39'),
        'class' => ['firewall-multi-adding__input'],
        'rows' => 10,
      ],
      '#theme_wrappers' => [],
    ];

    $form['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Add IP list'),
      '#attributes' => [
        'class' => ['button', 'firewall-multi-adding__save'],
      ],
      '#ajax' => [
        'callback' => '::ajaxSubmitIp',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    // Country blocking.
    if($cacheData = Cache::getdata('getBlockedCountries', $host['id'])){
      $waf_data = $cacheData['data'];
    } else {
      $waf_data = WtotemAPI::getBlockedCountries($host['id']);
      Cache::setData(['getBlockedCountries' => $waf_data], $host['id']);
    }

    $blocked_countries_list = $waf_data['blockedCountries'];
    $regions = Helper::getRegions();

    foreach ($regions as $region){
      $countries = [];
      foreach ($region['countries'] as $country) {
        $country_name = Helper::getCountryName($country);
        $countries[$country] = '<img width="19" height="13" src="https://assets.wtotem.net/images/flags/' . strtolower($country) . '.png" alt="Flag of ' . $country_name . '"><p>' . $country_name . '</p>';
      }

      $form[$region['region']]['countries'] = [
          '#type' => 'checkboxes',
          '#options' => $countries,
          '#title' => '',
          '#default_value' => $blocked_countries_list,

          '#attributes' => [
              'class' => ['wt-checkbox', 'country-blocking-form__checkbox', 'country-checkbox'],
          ],
          '#label_attributes' => [
              'class' => ['country-blocking-form__country'],
          ],
      ];

    }

    $form['country_blocking_submit'] = [
        '#type' => 'button',
        '#value' => $this->t('save'),
        '#attributes' => [
            'class' => ['tbuton', 'country-blocking-form-save__btn'],
        ],
        '#ajax' => [
            'callback' => '::ajaxSubmitCountryBlocking',
            'progress' => [
                'type' => 'throbber',
            ],
        ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) { }

  /**
   * Submit country blocking.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxSubmitCountryBlocking(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();

    $host = WtotemAPI::siteInfo();
    $countries = $form_state->getValue('countries');
    $countries = array_filter( $countries, static function($var){return $var !== 0;} );
    $countries = array_values($countries);

    if(WtotemAPI::syncBlockedCountries($host['id'], $countries)){
      $waf_data = WtotemAPI::getBlockedCountries($host['id']);
      Cache::setData(['getBlockedCountries' => $waf_data], $host['id']);

      App::setNotification('success', $this->t('Your changes have been applied successfully.'));
      $ajax_response->addCommand(new HtmlCommand('#num-of-countries', (string) count($waf_data['blockedCountries'])));
    } else {

      App::setNotification('success', $this->t('Your changes have not been applied.'));
    }

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Toggle WAF notification.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxToggleNotificationWaf() {
    $ajax_response = new AjaxResponse();

    $host = WtotemAPI::siteInfo();
    $configs = App::getSessionOption('configs');
    WtotemAPI::toggleNotifications($host['id'], $configs['waf']['service']);

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Toggle WAF module.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxToggleModuleWaf() {
    $ajax_response = new AjaxResponse();

    $configs = App::getSessionOption('configs');
    WtotemAPI::toggleConfigs($configs['waf']['id']);

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Toggle AV module.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxToggleModuleAv() {
    $ajax_response = new AjaxResponse();

    $configs = App::getSessionOption('configs');
    WtotemAPI::toggleConfigs($configs['av']['id']);

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Toggle server status module.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxToggleModuleSs() {
    $ajax_response = new AjaxResponse();

    $configs = App::getSessionOption('configs');
    WtotemAPI::toggleConfigs($configs['ss']['id']);

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Toggle WA module.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxToggleModuleWa() {
    $ajax_response = new AjaxResponse();

    $configs = App::getSessionOption('configs');
    WtotemAPI::toggleConfigs($configs['wa']['id']);

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Toggle deface module.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxToggleModuleDc() {
    $ajax_response = new AjaxResponse();

    $configs = App::getSessionOption('configs');
    WtotemAPI::toggleConfigs($configs['dc']['id']);

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Toggle reputation module.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxToggleModuleRc() {
    $ajax_response = new AjaxResponse();

    $configs = App::getSessionOption('configs');
    WtotemAPI::toggleConfigs($configs['rc']['id']);

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Toggle port scanner module.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxToggleModulePs() {
    $ajax_response = new AjaxResponse();

    $configs = App::getSessionOption('configs');
    WtotemAPI::toggleConfigs($configs['ps']['id']);

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Toggle technologies module.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxToggleModuleCms() {
    $ajax_response = new AjaxResponse();

    $configs = App::getSessionOption('configs');
    WtotemAPI::toggleConfigs($configs['cms']['id']);

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Toggle WA notification.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxToggleNotificationWa() {
    $ajax_response = new AjaxResponse();

    $host = WtotemAPI::siteInfo();
    $configs = App::getSessionOption('configs');
    WtotemAPI::toggleNotifications($host['id'], $configs['wa']['service']);

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Toggle AV notification.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxToggleNotificationAv() {
    $ajax_response = new AjaxResponse();

    $host = WtotemAPI::siteInfo();
    $configs = App::getSessionOption('configs');
    WtotemAPI::toggleNotifications($host['id'], $configs['av']['service']);

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Toggle deface notification.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxToggleNotificationDc() {
    $ajax_response = new AjaxResponse();

    $host = WtotemAPI::siteInfo();
    $configs = App::getSessionOption('configs');
    WtotemAPI::toggleNotifications($host['id'], $configs['dc']['service']);

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Toggle reputation notification.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxToggleNotificationRc() {
    $ajax_response = new AjaxResponse();

    $host = WtotemAPI::siteInfo();
    $configs = App::getSessionOption('configs');
    WtotemAPI::toggleNotifications($host['id'], $configs['rc']['service']);

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Toggle port scanner notification.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxToggleNotificationPs() {
    $ajax_response = new AjaxResponse();

    $host = WtotemAPI::siteInfo();
    $configs = App::getSessionOption('configs');
    WtotemAPI::toggleNotifications($host['id'], $configs['ps']['service']);

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Add IP to allow list.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxSubmitAllowIp(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();

    $ip = $form_state->getValue('allow_ip');

    $host = WtotemAPI::siteInfo();
    $response = WtotemAPI::addIpToList($host['id'], $ip, 'white');

    if ($response) {

	    $data = WtotemAPI::getIpLists($host['id']);
	    Cache::setData(['getIpLists' => $data], $host['id']);

      $build[] = [
        "#list" => Helper::getIpList($data['whiteList'], 'ip_allow'),
        '#theme' => 'wtotem_allow_deny_list',
      ];

      $ajax_response->addCommand(new HtmlCommand('#wtotem_allow_list', $build));
      $ajax_response->addCommand(new InvokeCommand('input[name=allow_ip]', 'val', ['']));

    }

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Add IP to deny list.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxSubmitDenyIp(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();

    $ip = $form_state->getValue('deny_ip');

    $host = WtotemAPI::siteInfo();
    $response = WtotemAPI::addIpToList($host['id'], $ip, 'black');

    if ($response) {

	    $data = WtotemAPI::getIpLists($host['id']);
	    Cache::setData(['getIpLists' => $data], $host['id']);

      $build[] = [
        "#list" => Helper::getIpList($data['blackList'], 'ip_deny'),
        '#theme' => 'wtotem_allow_deny_list',
      ];

      $ajax_response->addCommand(new HtmlCommand('#wtotem_deny_list', $build));
      $ajax_response->addCommand(new InvokeCommand('input[name=deny_ip]', 'val', ['']));
    }

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Add URL to allow list.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxSubmitAllowUrl(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();

    $url = $form_state->getValue('allow_url');

    $host = WtotemAPI::siteInfo();
    $response = WtotemAPI::addUrlToAllowList($host['id'], $url);

    if ($response) {

	    $data = WtotemAPI::getAllowUrlList($host['id']);
	    Cache::setData(['getAllowUrlList' => $data], $host['id']);

      $build[] = [
        "#list" => Helper::getUrlAllowList($data),
        '#theme' => 'wtotem_allow_url_list',
      ];

      $ajax_response->addCommand(new HtmlCommand('#wtotem_allow_url', $build));
      $ajax_response->addCommand(new InvokeCommand('input[name=allow_url]', 'val', ['']));
    }

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Config firewall settings.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();

    $has_error = false;
    $dos = $form_state->getValue('dos');
    $dos_limit = $form_state->getValue('dos_limit');
    $login_attempt = $form_state->getValue('login_attempt');
    $login_attempt_limit = $form_state->getValue('login_attempt_limit');

    $error_text = [];
    if($dos){
      if(empty($dos_limit)){
        $error_text['dos-limit'] = $this->t('The field is required.');
        $has_error = true;
      } else if($dos_limit < 500 or $dos_limit > 10000) {
        $error_text['dos-limit'] = sprintf($this->t('Please specify a value from %s to %s.'), '500', '10 000');
        $has_error = true;
      }
    }

    if($login_attempt){
      if(empty($login_attempt_limit)){
        $error_text['login-attempt-limit'] = $this->t('The field is required.');
        $has_error = true;
      } else if($login_attempt_limit < 5 or $login_attempt_limit > 30) {
        $error_text['login-attempt-limit'] = sprintf($this->t('Please specify a value from %s to %s.'), '5', '30');
        $has_error = true;
      }
    }

    $ajax_response->addCommand(new RemoveCommand('.wtotem_error_waf_settings'));
    $ajax_response->addCommand(new InvokeCommand('#wtotem-waf-settings input', 'removeClass', ['wtotem_input_error']));

    if($has_error){
      foreach ($error_text as $key => $value){
        $ajax_response->addCommand(new InvokeCommand('#edit-' . $key, 'addClass', ['wtotem_input_error']));
        $ajax_response->addCommand(new AppendCommand('.form-item--' . $key, '<div class="is--status--error wtotem-mb-15 wtotem_error_waf_settings">'. $value .'</div>'));
      }
    }

    if(!$has_error){
      $settings = [
          'gdn' => $form_state->getValue('gdn') ? 'true' : 'false',
          'dosProtection' => $dos ? 'true' : 'false',
          'dosLimit' => $dos_limit,
          'loginAttemptsProtection' => $login_attempt ? 'true' : 'false',
          'loginAttemptsLimit' => $login_attempt_limit,
      ];

      $host = WtotemAPI::siteInfo();
      $response = WtotemAPI::setFirewallSettings($host['id'], $settings);

      if (!$response['errors']) {

        $data = WtotemAPI::getFirewall($host['id']);
        Cache::setData(['getFirewall' => $data], $host['id']);

        App::setNotification('success', $this->t('Your changes have been applied successfully.'));
      }
    }

    WTotemAjaxController::notifications($ajax_response);

    return $ajax_response;
  }

  /**
   * Request to add a list of IP addresses.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxSubmitIp(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();

    $ips = $form_state->getValue('ips');
    $list_name = $form_state->getValue('list');

    $host = WtotemAPI::siteInfo();
    $response = WtotemAPI::addIpToList($host['id'], $ips, $list_name);

    if ($response) {

	    $data = WtotemAPI::getIpLists($host['id']);
	    Cache::setData(['getIpLists' => $data], $host['id']);

      $data_list = ($list_name == 'white') ? $data['whiteList'] : $data['blackList'];
      $ip_list = ($list_name == 'white') ? 'ip_allow' : 'ip_deny';
      $wrap = ($list_name == 'white') ? '#wtotem_allow_list' : '#wtotem_deny_list';

      $build[] = [
        "#list" => Helper::getIpList($data_list, $ip_list),
        '#theme' => 'wtotem_allow_deny_list',
      ];

      $ajax_response->addCommand(new HtmlCommand($wrap, $build));

      if ($response['status'] == 0) {
        $ajax_response->addCommand(new HtmlCommand('.wtotem_input__success', $this->t('IP addresses success added')));
        $ajax_response->addCommand(new HtmlCommand('.wtotem_input__error_text', ''));
        $ajax_response->addCommand(new InvokeCommand('textarea[name=ips]', 'val', ['']));
      }
      else {
        $ajax_response->addCommand(new HtmlCommand('.wtotem_input__success', ''));
        $ajax_response->addCommand(new HtmlCommand('.wtotem_input__error_text', $this->t('Incorrect IP addresses')));
        $ajax_response->addCommand(new InvokeCommand('textarea[name=ips]', 'val', [implode("\n", $response['invalidIPs'])]));
      }

    }

    return $ajax_response;
  }

}
