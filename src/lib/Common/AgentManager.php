<?php

namespace Drupal\wtotem\lib\Common;

use Drupal\Core\File\FileSystemInterface;
use Drupal\wtotem\lib\Common\API as WtotemAPI;

/**
 * Agent Manager controller.
 *
 * Agent Manager is needed to create AM file,
 *   and to check whether am and other agents are installed.
 * The AM file creates WAV and AV files,
 *   and transmits information to the WebTotem platform and back.
 */
class AgentManager {

  /**
   * AM file install.
   *
   * Create AM file in the root of the site
   *   and save the data about it in the DB.
   *
   * @return bool
   *   TRUE if the AM file is successfully added.
   */
  public static function amInstall() {
    try {
      $host = WtotemAPI::siteInfo();
      $files = self::getAgentsFiles($host['id']);

      if ($files['amFilename']) {
        // Download file.

        $result = self::downloadFile($files);

        // If the file is downloaded, then we write the data to the DB.
        if ($result) {
          App::setOptions([
            'am_installed' => TRUE,
            'am_file' => $files['amFilename'],
            'waf_file' => $files['wafFilename'],
            'av_file' => $files['avFilename'],
          ]);

          self::generateMarkerFile($files['amFilename']);

          $message = t('Agent manager have been successfully installed');
          App::setNotification('success', $message);
        }
        else {
          $message = t('Check root folder permissions');
          App::setNotification('error', $message);
          return FALSE;
        }
      }

    }
    catch (\Exception $e) {
      App::setNotification('error', $e->getMessage());
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Get the AM file download link and agents (AV, WAF) files name.
   *
   * @param string $host_id
   *   Host id on WebTotem.
   *
   * @return array
   *   Agent Manager download link, names of agents
   */
  public static function getAgentsFiles($host_id) {
    $result = WtotemAPI::getAgentsFiles($host_id);
    if (!isset($result['data']['auth']['am']['install'])) {
      $file = ['amFilename' => NULL, 'downloadLink' => NULL];

      $message = t('Error generating the agent manager file.');
      App::setNotification('error', $message);
    }
    else {
      $file = $result['data']['auth']['am']['install'];
    }

    return $file;
  }

  /**
   * Check the existence of the service file and the record in the DB.
   *
   * @param string $service
   *   Service short name (am, av, waf).
   *
   * @return array
   *   Service file data (installed status, file exist status, file name).
   */
  public static function checkInstalledService($service) {
    $file = App::getOption($service . "_file");
    return [
      'option_status' => App::getOption($service . "_installed"),
      'file_status' => ((bool) $file) && is_file(DRUPAL_ROOT . '/' . $file),
      'file_name' => $file,
    ];
  }

  /**
   * Remove all agents files and folders. Clear agents options.
   *
   * @return bool
   *   Returns TRUE if agent files
   *   and folders have been successfully deleted.
   */
  public static function removeAgents() {
    // Deleting all agent records from the database.
    App::clearOptions([
      'am_installed',
      'waf_installed',
      'av_installed',
      'am_file',
      'waf_file',
      'av_file',
    ]);

    $list = scandir(DRUPAL_ROOT);

    foreach ($list as $name) {
      $path = DRUPAL_ROOT . '/' . $name;
      $pattern = '/([a-zA-Z0-9_]{64}.av.php)|([a-zA-Z0-9_]{64}.am.php)|([a-zA-Z0-9_]{64}.waf.php)|(wtotem_[a-zA-Z0-9_]{12,16})/';

      if (preg_match($pattern, $name)) {
        Helper::delete($path);
      }
    }
    return TRUE;
  }

    /**
     * Download AM file.
     *
     * @param array $files
     *   Link from where to download the file and Path where to save the file.
     *
     * @return bool
     *   If the file is saved successfully, it returns true.
     */
    private static function downloadFile($files) {

        try {
            $data = (string) \Drupal::httpClient()->get($files['downloadLink'])->getBody();
            return \Drupal::service('file_system')->saveData($data, DRUPAL_ROOT . '/' . $files['amFilename'], FileSystemInterface::EXISTS_REPLACE);
        }
        catch (TransferException $exception) {
            App::setNotification('success', t('Failed to fetch file due to error "%error"', ['%error' => $exception->getMessage()]));
    //            \Drupal::messenger()->addError(t('Failed to fetch file due to error "%error"', ['%error' => $exception->getMessage()]));
        }
        catch (FileException | InvalidStreamWrapperException $e) {
            App::setNotification('success', t('Failed to save file due to error "%error"', ['%error' => $e->getMessage()]));
    //            \Drupal::messenger()->addError(t('Failed to save file due to error "%error"', ['%error' => $e->getMessage()]));
        }
        return FALSE;

    }

  /**
   * Generate the file that indicates that a WAF connection is being used through the plugin.
   *
   * @param $am_filename
   *   AM file name
   */
  public static function generateMarkerFile($am_filename) {

    if (\Drupal::service('file_system')->createFilename(WTOTEM_ROOT_PATH, 'generate.php')) {
      $content = '<?php exit(); ?>' . $am_filename;
      file_put_contents(WTOTEM_ROOT_PATH . 'generate.php', $content);
    }
    else {
      $message = t('Check @path folder permissions', ['@path' => WTOTEM_MODULE_PATH]);
      App::setNotification('error', $message);
    }

  }

  /**
   * Check if the plugin version has changed.
   */
  public static function checkVersion(){
    // Get version of the plugin that was previously installed.
    $version = App::getOption('plugin_version');

    if ($version == WTOTEM_VERSION) {
      return;
    }

    App::setOptions(['plugin_version' => WTOTEM_VERSION]);

    // Generate the file that indicates that a WAF connection is being used through the plugin.
    if($am_file = App::getOption('am_file')){
      self::generateMarkerFile($am_file);
    }

  }

  /**
   * Checking whether agents are installed, if they are not installed, then install.
   */
  public static function checkAgents(){

    $api_key = App::getOption('api_key');

    // Check if the plugin version has changed.
    self::checkVersion();

    $host = API::siteInfo();

    if ($api_key && array_key_exists('id', $host)) {

      // Install Agent Manager if it was not previously installed.
      $am_installed = self::checkInstalledService('am');
      if (!$am_installed['option_status'] || !$am_installed['file_status']) {

        $am_was_installed = self::amInstall();

        if (!$am_was_installed) {
          App::setOptions(['am_installed' => FALSE]);
        }
      }

    }
  }

}
