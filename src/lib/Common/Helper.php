<?php

namespace Drupal\wtotem\lib\Common;

use Drupal\Core\Locale\CountryManager;
use Exception;

/**
 * WebTotem Helper class for Drupal.
 */
class Helper {

  /**
   * Check that the training period has passed for the firewall.
   *
   * @param string $created_at
   *   Date when the waf configuration was created.
   *
   * @return bool
   *   Returns boolean.
   */
  public static function isWafTraining($created_at) {
    $when_waf_trained = strtotime('+2 day', strtotime($created_at));
    $today = strtotime('today');

    return ($when_waf_trained < $today) ? FALSE : TRUE;
  }

  /**
   * Converting a date to the appropriate format.
   *
   * @param string $date
   *   Date in any format.
   * @param string $format
   *   The format to which you want to convert the date.
   *
   * @return string
   *   Returns converted Date.
   */
  public static function dateFormatter($date, $format = 'M j, Y \/ H:i') {
    if (!$date) {
      return t('Unknown');
    }

    $time_zone = App::getOption('time_zone_offset');
    $user_time = ($time_zone) ? strtotime($time_zone . 'hours', strtotime($date)) : strtotime($date);

    $date_formatter = \Drupal::service('date.formatter');
    return $date_formatter->format($user_time, 'custom', $format);
  }

  /**
   * Checks whether secure HTTPS (SSL) connection is used. .
   *
   * @return boolean
   *    The function returns true if SSL is used on the current page.
   */
  public static function isSsl() {
    if ( isset( $_SERVER['HTTPS'] ) ) {
      if ( 'on' === strtolower( $_SERVER['HTTPS'] ) ) {
        return true;
      }

      if ( '1' === (string) $_SERVER['HTTPS'] ) {
        return true;
      }
    } elseif ( isset( $_SERVER['SERVER_PORT'] ) && ( '443' === (string) $_SERVER['SERVER_PORT'] ) ) {
      return true;
    }

    return false;
  }

  /**
   * Checking whether the current domain belongs to the kz domain zone.
   *
   * @return bool
   *    true is returned if the domain belongs to the kz domain zone.
   */
  public static function isKz() {
    $is_kz = App::getOption('is_kz');
    if (is_array($is_kz)) {
      return $is_kz['value'];
    }

    if(function_exists('idn_to_utf')){
      $host = idn_to_utf8($_SERVER['HTTP_HOST']);
    } else {
      $host = $_SERVER['HTTP_HOST'];
    }

    $parts = explode('.', $host);
    $domain_zone = $parts[count($parts) - 1];

    if ($domain_zone === 'kz' or $domain_zone === 'қаз') {
      $is_kz['value'] = true;
    } else {
      $is_kz['value'] = false;
    }

    App::setOptions(['is_kz' => $is_kz]);

    return $is_kz['value'];
  }


    /**
   * Get theme mode data.
   *
   * @return array
   *   Returns array with current theme data.
   */
  public static function getThemeMode() {
    $theme_mode = App::getSessionOption('theme_mode');
    return [
      "is_dark_mode" => $theme_mode == 'dark' ? 'wtotem_theme—dark' : '',
      "dark_mode_checked" => $theme_mode == 'dark' ? 'checked' : '',
    ];
  }

  /**
   * Get current user language.
   *
   * @return string
   *   Returns current language in 2-letter abbreviations
   */
  public static function getLanguage() {
    $current_language = \Drupal::languageManager()->getCurrentLanguage();
    return (in_array($current_language->getId(),['ru','en','pl'])) ? $current_language->getId() : 'en';
  }

  /**
   * Converting a date to the appropriate format.
   *
   * @param string|array $days
   *   Number of days or period to convert.
   *
   * @return array
   *   Returns an array of two values "from" and "to"
   */
  public static function getPeriod($days) {

    if (!$days) {
      $days = 30;
    }

    switch ($days) {

      case is_array($days):
        $to = $days[1] ?: $days[0];
        $period = [
          'from' => strtotime(date('Y-m-d 00:00:01', strtotime($days[0]))),
          'to' => strtotime(date('Y-m-d 23:59:59', strtotime($to))),
        ];
        break;

      case $days <= 1:
        $period = [
          'from' => strtotime('-24 hours'),
          'to' => time(),
        ];
        break;

      default:
        $period = [
          'from' => time() - ($days * 86400),
          'to' => time(),
        ];
    }

    return $period;
  }

  /**
   * Convert an array to a string with quotation marks.
   *
   * @param array $array
   *   Data array.
   *
   * @return string
   *   Array of data converted to string.
   */
  public static function convertArrayToString($array) {
    if(empty($array)){
      return '';
    }
    return '"' . implode('","', $array) . '"';
  }


  /**
   * Converting the response to a readable form.
   *
   * @param string $message
   *   Message response from the API server to the request.
   *
   * @return string|bool
   *   Returns a message.
   */
  public static function messageForHuman($message) {
    $definition = $message;
    $excepts = [
      'RESOURCE_NOT_FOUND',
      'DUPLICATE_HOST',
      'INVALID_CREDENTIALS',
      'INVALID_API_KEY',
      'Invalid token',
      'PORT_ALREADY_ADDED',
    ];
    if (in_array($message, $excepts)) {
      return FALSE;
    }
    switch ($message) {
      case 'HOSTS_LIMIT_EXCEEDED':
        $definition = t('Limit of adding sites exceeded.');
        break;

      case 'Invalid token':
        $definition = t('Token expired.');
        break;

      case 'USER_ALREADY_REGISTERED':
        $definition = t('A user with this email already exists.');
        break;

      case 'DUPLICATE_HOST':
        $definition = t('Duplicate host');
        break;

      case 'INVALID_DOMAIN_NAME':
        $definition = t('Invalid Domain Name');
        break;
    }
    return $definition;
  }

  /**
   * Get the data associated with the status.
   *
   * @param string $status
   *   Module or agent status.
   *
   * @return array
   *   Returns an array with status data.
   */
  public static function getStatusData($status) {
    $path = WTOTEM_MODULE_PATH;

    switch ($status) {

      case 'clean':
      case 'up':
      case 'installed':
      case 'working':
      case 'good':
        $status_data = [
          'class' => 'is--status--ok',
          'image' => $path . '/img/check-mark.svg',
          'icon' => $path . '/img/icon_success_status.svg',
        ];
        break;

      case 'pending':
        $status_data = [
          'class' => 'is--status--pending',
          'image' => $path . '/img/loading.svg',
          'icon' => $path . '/img/alert-warning.svg',
        ];
        break;

      case 'pause':
      case 'modified':
        $status_data = [
          'class' => 'is--status--pending',
          'image' => $path . '/img/warning.svg',
          'icon' => $path . '/img/alert-warning.svg',
        ];
        break;

      case 'expired':
      case 'no_cert':
      case 'expires':
      case 'open_ports':
      case 'warning':
      case 'not_supported':
      case 'not_registered':
        $status_data = [
          'class' => 'is--status--warning',
          'image' => $path . '/img/warning.svg',
          'icon' => $path . '/img/alert-warning.svg',
        ];
        break;

      case 'invalid':
      case 'revoked':
      case 'untrusted':
      case 'not_found':
      case 'wrong_host':
      case 'error':
      case 'down':
      case 'expires_today':
      case 'infected':
      case 'deface':
      case 'not_installed':
        $status_data = [
          'class' => 'is--status--error',
          'image' => $path . '/img/warning.svg',
          'icon' => $path . '/img/alert-warning.svg',
        ];
        break;

      default:
        $status_data = [
          'class' => 'is--status--pending',
          'image' => $path . '/img/warning.svg',
          'icon' => $path . '/img/alert-warning.svg',
        ];
    }
    $status_data['name'] = $status;
    $status_data['text'] = self::getStatusText($status);
    $status_data['tooltips'] = self::getTooltips($status);

    return $status_data;
  }

  /**
   * Get a readable status text.
   *
   * @param string $status
   *   Module or agent status.
   *
   * @return string
   *   Returns the status text in the current language.
   */
  public static function getStatusText($status) {
    $statuses = [
      'warning' => t('Warning'),
      'error' => t('Error'),
      'success' => t('Success'),
      'info' => t('Info'),
      'invalid' => t('Invalid'),
      'ok' => t('Everything is OK'),
      'expired' => t('Expired'),
      'expires' => t('Expires'),
      'expires_today' => t('Expires today'),
      'missing' => t('Missing'),
      'active' => t('Active'),
      'inactive' => t('Inactive'),
      'pending' => t('Pending'),
      'pause' => t('Disabled'),
      'available' => t('Available'),
      'not_supported' => t('Not supported'),
      'not_registered' => t('Not registered'),
      'unsupported' => t('Unsupported'),
      'clean' => t('Clean'),
      'clear' => t('Clear'),
      'blacklisted' => t('Infected'),
      'miner_detected' => t('Infected'),
      'deface' => t('Deface'),
      'modified' => t('Modified'),
      'detected' => t('Detected'),
      'open_ports' => t('Open ports'),
      'blocked' => t('Blocked'),
      'connected' => t('Connected'),
      'attacks_detected' => t('Attacks detected'),
      'signature_found' => t('Signature found'),
      'file_changes' => t('File changes'),
      'no_cert' => t('No cert'),
      'down' => t('Down'),
      'up' => t('Up'),
      'infected' => t('Infected'),
      'not_installed' => t('Need to install'),
      'working' => t('Working'),
      "critical" => t('Critical'),
      "deleted" => t('Deleted'),
      "changed" => t('Changed'),
      "new" => t('New'),
      "scanned" => t('Scanned'),
      "quarantine" => t('In quarantine'),
      "good" => t('Good'),
      "wrong_host" => t('Wrong host'),
      "revoked" => t('Revoked'),
      "untrusted" => t('Untrusted'),
      "not_found" => t('Not found'),
    ];

    return (array_key_exists($status, $statuses)) ? $statuses[$status] : $status;
  }

  /**
   * Get tooltips text for status.
   *
   * @param string $status
   *   Module or agent status.
   *
   * @return string
   *   Returns the status tooltip in the current language.
   */
  public static function getTooltips($status) {
    $tooltips = [
      'invalid' => t('<b>Invalid</b> -The certificate is invalid. Please, make sure that relevant certificate details filled correctly.'),
      'expired' => t('<b>Expired</b> - The certificate has expired. Connection is not secure. Please, renew it.'),
      'expires' => t('<b>Expires</b> - The certificate expires soon. Please, take actions.'),
      'expires_today' => t('<b>Expires today</b> - The certificate expires today. Please, take actions.'),
      'error' => t("<b>Error</b> - Something went wrong. Please, contact us, we'll fix the problem."),
      'pending' => t('<b>Pending</b> - System processes your website. Data will be available soon.'),
      'pause' => t('<b>Pause</b> - The module is paused.'),
      'clean' => t('<b>Everything is OK</b> - Nothing to worry about. Everything is alright.'),
      'deface' => t("<b>Deface</b> - Website hacked. Please, contact us, we'll fix the problem."),
      'open_ports' => t('<b>Open ports</b> - Open ports detected. Your website is vulnerable to attacks.'),
      'blocked' => t('<b>Blocked</b> - The module is blocked due to billing issues.'),
      'no_cert' => t("<b>No cert</b> - You don't have SSL certificate. We recommend you to install it for security concerns."),
      'down' => t('<b>Down</b> - The website is not available for visitors.'),
      'up' => t('<b>Up</b> - The website is available for visitors.'),
      'infected' => t('<b>Infected</b> - The website site is blacklisted and may have infected files. Please, check antivirus module.'),
    ];

    return (array_key_exists($status, $tooltips)) ? $tooltips[$status] : '';
  }

  /**
   * Get reports with modules list.
   *
   * @param array $edges
   *   Data on generated reports.
   *
   * @return array
   *   Returns an array with converted data.
   */
  public static function getReports($edges) {
    $modulesLang = [
      'wa' => t('Availability log'),
      'dc' => t('Deface log'),
      'ps' => t('Port log'),
      'rc' => t('Reputation log'),
      'sc' => t('Evaluation log'),
      'av' => t('Antivirus log'),
      'waf' => t('Firewall log'),
    ];

    $reports = [];

    foreach ($edges as $edge) {
      if (in_array(FALSE, $edge["node"])) {
        $arr = [];
        foreach ($edge["node"] as $module => $value) {
          if ($value && array_key_exists($module, $modulesLang)) {
            $arr[] = $modulesLang[$module];
          }
        }
        $modules = implode(", ", $arr);
      }
      else {
        $modules = t('All modules');
      }

      $reports[] = [
        'id' => $edge["node"]['id'],
        'modules' => $modules,
        'created_at' => self::dateFormatter($edge["node"]['createdAt']),
      ];
    }

    return $reports;
  }

  /**
   * Get reputation status description.
   *
   * @param string $status
   *   Reputation status.
   *
   * @return string
   *   Returns a description of the reputation status
   */
  public static function getReputationInfo($status) {
    switch ($status) {
      case 'clean':
        $data = t("Don't worry, your reputation is good");
        break;

      case 'infected':
        $data = t('Oh, your reputation is bad');
        break;

      default:
        $data = t('Information is being updated');
    }
    return $data;
  }

  /**
   * Get blacklists entries counts.
   *
   * @param string $status
   *   Reputation status.
   * @param array $virus_list
   *   Sources where the site can be blacklisted.
   *
   * @return int
   *   Number of references in blacklists.
   */
  public static function blacklistsEntries($status, $virus_list) {
    $count = 0;
    if ($status != "clean") {
      foreach ($virus_list as &$list) {
        if (!empty($list['virus']['type'])) {
          $count++;
        }
      }
    }
    return $count;
  }

  /**
   * Classification of the rating in the letter grades.
   *
   * @param int $score
   *   Site rating from 1 to 100.
   *
   * @return array
   *   Returns an array of data.
   */
  public static function scoreGrading($score) {
    if ($score < 0 || $score > 100) {
      return ['grade' => '', 'color' => ''];
    }

    $scores = [
      100 => 'A+',
      90 => 'A',
      80 => 'A-',
      70 => 'B+',
      60 => 'B',
      50 => 'B-',
      35 => 'C+',
      20 => 'C',
      0 => 'C-',
    ];

    foreach ($scores as $key => $value) {
      if ($score >= $key) {
        $grade = $value;
        break;
      }
    }

    // Set a color depending on the grade.
    switch ($score) {
      case $score >= 80:
        $color = 'green';
        break;

      case $score >= 50:
        $color = 'orange';
        break;

      default:
        $color = 'red';
    }

    return ['grade' => $grade, 'color' => $color];
  }

  /**
   * Calculate the number of remaining days.
   *
   * @param string $date
   *   Expiry date.
   *
   * @return string
   *   Returns the number of days before the expiration date.
   * @throws Exception
   */
  public static function daysLeft($date) {
    if ((int) $date === 0) {
      $days_left = 0;
    }
    else {
      $now = new \DateTime();
      $expiry_date = new \DateTime();
      $timestamp = strtotime($date);
      $expiry_date->setTimestamp($timestamp);
      $days_left = $expiry_date->diff($now)->format("%a");
    }
    return $days_left;
  }

  /**
   * Reassembling the firewall logs.
   *
   * @param array $logs_
   *   Firewall logs from WebTotem.
   *
   * @return array
   *   Reassembled array of logs.
   */
  public static function wafLogs( $logs_) {
    $logs = [];
    foreach ($logs_ as $key => $log) {
      $log = $log['node'];

      $logs[$key]['ip'] = $log['ip'];
      $logs[$key]['request'] = htmlspecialchars(urldecode($log['request']));
      $logs[$key]['time'] = self::dateFormatter($log['time']);
      $logs[$key]['country_code'] = strtolower($log['country']);
      $logs[$key]['country'] = $log['location']['country']['nameEn'];
      $logs[$key]['blocked'] = $log['blocked'] ? t('Blocked IP') : t('Not blocked');

      $more = [
          'ip' => $log['ip'],
          'proxy_ip' => $log['proxyIp'],
          'source' => $log['source'],
          'request' => htmlspecialchars(urldecode($log['request'])),
          'user_agent' => $log['userAgent'],
          'time' => self::dateFormatter($log['time']),
          'type' => $log['type'],
          'category' => $log['category'],
          'country' => $log['location']['country']['nameEn'],
          'payload' => htmlspecialchars(urldecode($log['payload'])),
      ];

      $logs[$key]['more'] = json_encode($more);

    }
    return $logs;
  }

  /**
   * Converting firewall data to json for a D3 chart.
   *
   * @param array $charts
   *   Charts data from WebTotem.
   *
   * @return array
   *   Returns the converted data for chart.
   */
  public static function generateWafChart($charts) {
    $sum = 0;
    foreach ($charts as $chart) {
      $sum += $chart['attacks'];
    }
    if ($sum == 0) {
      return ['chart' => FALSE, 'count_attacks' => 0, 'count_blocks' => 0];
    }

    // Get days count.
    $charts_ = $charts;
    $first = array_shift($charts_);
    $last = array_pop($charts_);
    $days = ceil((strtotime($last['time']) - strtotime($first['time'])) / 86400);

    // Set variables.
    $count_attacks = $count_blocks = 0;

    foreach ($charts as $chart) {
      if ($days <= 1) {
        $time_zone = App::getOption('time_zone_offset');
        $userTime = ($time_zone) ? strtotime($time_zone . ' hours', strtotime($chart['time'])) : strtotime($chart['time']);
      }
      if (($chart['attacks'] and $days == 2) or $days != 2) {
        $result[] = [
          'date' => ($days <= 1) ? date("Y-m-d H:00:00", $userTime) : date("Y-m-d", strtotime($chart['time'])),
          'count' => $chart['blocked'],
          'attacks' => $chart['attacks'],
          'blocked' => $chart['blocked'],
        ];
        $count_attacks += $chart['attacks'];
        $count_blocks += $chart['blocked'];
      }
    }

    if (!isset($result)) {
      return [
        'chart' => FALSE,
        'count_attacks' => 0,
        'count_blocks' => 0,
      ];
    }

    return [
      'chart' => json_encode($result),
      'count_attacks' => $count_attacks,
      'count_blocks' => $count_blocks,
      'days' => $days,
    ];
  }

  /**
   * Converting data to json for a D3 chart.
   *
   * @param array $charts
   *   Charts data from WebTotem.
   * @param int $days
   *   The number of days to build the chart.
   *
   * @return bool|string
   *   Returns the converted data for chart.
   */
  public static function generateChart($charts, $days = 7) {
    $sum = 0;
    foreach ($charts as $chart) {
      $sum += $chart['value'];
    }
    if ($sum == 0) {
      return FALSE;
    }

    $result = [];

    foreach ($charts as $chart) {
      if ($days <= 1) {
        $time_zone = App::getOption('time_zone_offset');
        $userTime = ($time_zone) ? strtotime($time_zone . 'hours', strtotime($chart['time'])) : strtotime($chart['time']);
      }
      $result[] = [
        'date' => ($days <= 1) ? date("Y-m-d H:00:00", $userTime) : date("Y-m-d", strtotime($chart['time'])),
        'value' => $chart['value'],
      ];
    }

    return json_encode($result, TRUE);
  }

  /**
   * Converting data to json for a D3 chart.
   *
   * @param array $data
   *   Charts data from WebTotem.
   *
   * @return array|bool
   *   Returns the converted data for chart.
   */
  public static function generateAttacksMapChart($data) {
    $attacks = [];
    $countries = [];
    foreach ($data as $value) {
      $attacks[] = $value['attacks'];
      $labels[] = self::getCountryName($value['country']);
      $countries[] = $value['location']['country']['nameEn'];
    }
    $result = ['attacks' => $attacks, 'countries' => $countries, 'labels' => $labels];

    if (!$attacks) {
      return FALSE;
    }

    return json_encode($result, TRUE);
  }

  /**
   * Reassembling the antivirus logs.
   *
   * @param array $logs_
   *   Antivirus logs from WebTotem.
   *
   * @return array
   *   Reassembled array of logs.
   */
  public static function getAntivirusLogs($logs_) {
    $logs = [];
    foreach ($logs_ as $key => $log) {
      $log = $log['node'];

      $filePath = str_replace("%", "*1", $log['filePath']);
      $log['original_path'] = str_replace("/", "*2", $filePath);
      $log['file_path'] = urldecode($log['filePath']);
      $log['time'] = self::dateFormatter($log['time']);
      $log['permissions_changed'] = $log['permissionsChanged'];
      $log['status'] = self::getStatusData($log['event']);
      $log['class_path'] = '';

      switch ($log['event']) {
        case 'modified':
        case 'quarantine':
          $log['class_path'] = "wtotem_file-table__td_changed";
          break;

        case 'infected':
          $log['class_path'] = "wtotem_file-table__td_critical";
          break;
      }

      $logs[$key] = $log;
    }
    return $logs;
  }

  /**
   * Get open path data.
   *
   * @param array $ports
   *   Open ports array.
   *
   * @return array
   *   Reassembled array of open ports.
   */
  public static function getOpenPortsData($ports) {
    if(!$ports){
      return [];
    }
    foreach ($ports as $key => $port) {
      $summary = '';
      if($port['cveList']){
        foreach ($port['cveList'] as $item){
          $summary .= '<p>' . $item['summary'] . '</p>';
        }
      }
      $ports[$key]['cve_summary'] = $summary;
    }
    return $ports;
  }

  /**
   * Reassembling the quarantine logs.
   *
   * @param array $logs_
   *   Quarantine logs from WebTotem.
   *
   * @return array
   *   Reassembled array of logs.
   */
  public static function getQuarantineLogs($logs_) {
    $logs = [];
    foreach ($logs_ as $key => $log) {
      $logs[$key] = $log;
      $logs[$key]['path'] = urldecode($log['path']);
      $logs[$key]['date'] = self::dateFormatter($log['date']);
    }

    return $logs;
  }

  /**
   * Generate an array of IP address data.
   *
   * @param array $data
   *   IP addresses data from WebTotem.
   * @param string $list_name
   *   Allow or deny list.
   *
   * @return array
   *   Returns array of data.
   */
  public static function getIpList($data, $list_name) {
    $list = [];
    foreach ($data as $item) {
      $list[] = [
        'ip' => $item['ip'],
        'id' => $item['id'],
        'created_at' => self::dateFormatter($item['createdAt']),
        'list_name' => $list_name,
      ];
    }
    return $list;
  }

  /**
   * Generate an array of URL address data.
   *
   * @param array $data
   *   URL addresses data from WebTotem.
   *
   * @return array
   *   Returns array of data.
   */
  public static function getUrlAllowList($data) {
    $list = [];
    foreach ($data as $item) {
      $list[] = [
        'url' => $item['url'],
        'id' => $item['id'],
        'created_at' => self::dateFormatter($item['createdAt']),
        'list_name' => 'url_allow',
      ];
    }
    return $list;
  }

  /**
   * Convert IP list to be transferred to WebTotem.
   *
   * @param string $data
   *   IP list.
   *
   * @return string
   *   Returns the converted string.
   */
  public static function convertIpListForApi($data) {
    if (!$data) {
      return FALSE;
    }

    $ips = preg_split("/(?(?=[\s,])[^.]|^$)/", $data);

    if (is_array($ips)) {
      $ips_ = '[';
      foreach ($ips as $ip) {
        if (!empty($ip)) {
          $ips_ .= '"' . $ip . '",';
        }
      }
      $ips_ = substr($ips_, 0, -1);
      $ips_ .= ']';
    }
    else {
      $ips_ = '"' . $ips . '"';
    }

    return $ips_;
  }

  /**
   * Get data of the country with the most attacks.
   *
   * @param array $map
   *   Map logs from WebTotem.
   * @param int $count_attacks
   *   Total attacks.
   *
   * @return array
   *   Returns array of data.
   */
  public static function getMostAttacksData($map) {

    if ($map) {
      $most_attacks_key = array_search(max(array_column($map, 'attacks')), array_column($map, 'attacks'));
      $total_attacks = array_sum(array_column($map, 'attacks'));

      $data['percent'] = ($total_attacks) ? round($map[$most_attacks_key]['attacks'] / $total_attacks * 100) : 0;
      $data['country'] = self::getCountryName($map[$most_attacks_key]['country']);
      $data['offset'] = 176 / 100 * (100 - $data['percent']);

      return $data;
    }

    return ['percent' => 0, 'country' => FALSE, 'offset' => 0];
  }

  /**
   * Get data on the three most attacking countries.
   *
   * @param array $map
   *   Map logs from WebTotem.
   *
   * @return array
   *   Returns array of data.
   */
  public static function getTreeMostAttacksData($map) {
    $total_attacks = array_sum(array_column($map, 'attacks'));

    if ($map) {
      array_multisort (array_column($map, 'attacks'), SORT_DESC, $map);
      $data = array_slice($map, 0, 3);

      foreach ($data as $key => $value){
        $data[$key]['percent'] = round($value['attacks'] / $total_attacks * 100);
        $data[$key]['country'] = self::getCountryName($value['country']);
      }

      return $data;
    }

    return [];
  }

  /**
   * Getting the country name by two-letter code.
   *
   * @param string $key
   *   Two-letter code.
   *
   * @return string
   *   Returns country name.
   */
  public static function getCountryName($key) {
    $countries = CountryManager::getStandardList();
    $key = (string) $key;

    return (array_key_exists($key, $countries)) ? $countries[$key] : $key;
  }

  /**
   * Get blocked countries count.
   *
   * @param array $blocked_countries_array
   *   Blocked countries array from WebTotem.
   *
   * @return array
   *   Returns array of data.
   */
  public static function getBlockedCountriesCount($blocked_countries_array) {

    $regions = self::getRegions();

    $counts = [
        'all' => count($blocked_countries_array),
        'Asia' => 0,
        'Europe' => 0,
        'Africa' => 0,
        'Oceania' => 0,
        'Americas' => 0
    ];

    foreach ($regions as $region){
      foreach ($region['countries'] as $country) {
        if(in_array($country, $blocked_countries_array)){
          $counts[$region['region']]++;
        }
      }
    }

    return $counts;
  }

  /**
   * Replace array indexes by key.
   *
   * @param array $array
   *   Original array.
   * @param string $key
   *   The key to use as an index.
   *
   * @return array
   *   Returns a new array.
   */
  public static function arrayMapIndex(array $array, $key) {
    $new_array = [];
    foreach ($array as $item) {
      if (array_key_exists($key, $item)) {
        $new_array[$item[$key]] = $item;
      }
    }
    return $new_array;
  }

  /**
   * Deleting a file or directory with all its contents.
   *
   * @param string $path
   *   Link to a file or directory.
   */
  public static function delete($path) {
    if (is_dir($path)) {
      \Drupal::service('file_system')->deleteRecursive($path);
    }
    else {
      \Drupal::service('file_system')->delete($path);
    }
  }

  /**
   * Generate random string.
   *
   * @param int $length
   *   The required length of the string.
   *
   * @return string
   *   Returns random string.
   */
  public static function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  /**
   * Get notifications array.
   *
   * @return array
   *   Returns notifications array.
   */
  public static function getNotifications() {

    $notifications_data = App::getNotifications();
    $notifications = [];

    foreach ($notifications_data as $notification) {
      switch ($notification['type']) {
        case 'error':
          $image = 'alert-error.svg';
          $class = 'wtotem_alert__title_red';
          break;

        case 'warning':
          $image = 'alert-warning.svg';
          $class = 'wtotem_alert__title_yellow';
          break;

        case 'success':
          $image = 'alert-success.svg';
          $class = 'wtotem_alert__title_green';
          break;

        case 'info':
          $image = 'info-blue.svg';
          $class = 'wtotem_alert__title_blue';
          break;
      }

      $notifications[] = [
        "text" => $notification['notice'],
        "id" => self::generateRandomString(8),
        "type" => self::getStatusText($notification['type']),
        "type_raw" => $notification['type'],
        "image" => $image,
        "class" => $class,
      ];
    }

    return $notifications;
  }

  /**
   * Get current agent installation statuses.
   *
   * @param array $agents_statuses
   *   Agents statuses got from the WebTotem API.
   *
   * @return array
   *   Returns an array with agent installation status data.
   */
  public static function getAgentsStatuses($agents_statuses) {
    $agents = ['am', 'waf', 'av'];
    $installing_statuses = [
      'not_installed',
      'installing',
      'internal_error',
      'update_error',
      'config_error',
      'session_error',
    ];

    $process_statuses = [];
    $option_statuses = [];

    foreach ($agents as $agent) {
      $status = AgentManager::checkInstalledService($agent);
      $option_statuses[$agent] = $status['option_status'] ?: FALSE;

      if ($agent == 'am') {
        if ($status['file_status']) {
          $process_statuses[$agent] = 'installed';
        }
        else {
          $process_statuses[$agent] = 'failed';
        }
      }
      else {
        if ($status['file_status']) {
          if (in_array($agents_statuses[$agent], $installing_statuses)) {
            $process_statuses[$agent] = 'installing';
          }
          elseif ($agents_statuses[$agent] == 'agent_not_available') {
            $process_statuses[$agent] = 'failed';
          }
          else {
            $process_statuses[$agent] = 'installed';
          }
        }
        else {
          $process_statuses[$agent] = 'installing';
        }
      }
    }

    return [
      'process_statuses' => $process_statuses,
      'option_statuses' => $option_statuses,
    ];
  }

  public static function getRegions(){
    return [
        [
            'region' => "Asia",
            'countries' => [
                "AF",
                "AM",
                "AZ",
                "BH",
                "BD",
                "BT",
                "BN",
                "KH",
                "CN",
                "CY",
                "GE",
                "HK",
                "IN",
                "ID",
                "IR",
                "IQ",
                "IL",
                "JP",
                "JO",
                "KZ",
                "KP",
                "KR",
                "KW",
                "KG",
                "LA",
                "LB",
                "MO",
                "MY",
                "MV",
                "MN",
                "MM",
                "NP",
                "OM",
                "PK",
                "PS",
                "PH",
                "QA",
                "SA",
                "SG",
                "LK",
                "SY",
                "TW",
                "TJ",
                "TH",
                "TL",
                "TR",
                "TM",
                "AE",
                "UZ",
                "VN",
                "YE"
            ]
        ],
        [
            'region' => "Europe",
            'countries' => [
                "AX",
                "AL",
                "AD",
                "AT",
                "BY",
                "BE",
                "BA",
                "BG",
                "HR",
                "CZ",
                "DK",
                "EE",
                "FO",
                "FI",
                "FR",
                "DE",
                "GI",
                "GR",
                "GG",
                "VA",
                "HU",
                "IS",
                "IE",
                "IM",
                "IT",
                "JE",
                "LV",
                "LI",
                "LT",
                "LU",
                "MT",
                "MD",
                "MC",
                "ME",
                "NL",
                "MK",
                "NO",
                "PL",
                "PT",
                "RO",
                "RU",
                "SM",
                "RS",
                "SK",
                "SI",
                "ES",
                "SJ",
                "SE",
                "CH",
                "UA",
                "GB"
            ]
        ],
        [
            'region' => "Africa",
            'countries' => [
                "DZ",
                "AO",
                "BJ",
                "BW",
                "IO",
                "BF",
                "BI",
                "CV",
                "CM",
                "CF",
                "TD",
                "KM",
                "CG",
                "CD",
                "CI",
                "DJ",
                "EG",
                "GQ",
                "ER",
                "SZ",
                "ET",
                "TF",
                "GA",
                "GM",
                "GH",
                "GN",
                "GW",
                "KE",
                "LS",
                "LR",
                "LY",
                "MG",
                "MW",
                "ML",
                "MR",
                "MU",
                "YT",
                "MA",
                "MZ",
                "NA",
                "NE",
                "NG",
                "RE",
                "RW",
                "SH",
                "ST",
                "SN",
                "SC",
                "SL",
                "SO",
                "ZA",
                "SS",
                "SD",
                "TZ",
                "TG",
                "TN",
                "UG",
                "EH",
                "ZM",
                "ZW"
            ]
        ],
        [
            'region' => "Oceania",
            'countries' => [
                "AS",
                "AU",
                "CX",
                "CC",
                "CK",
                "FJ",
                "PF",
                "GU",
                "HM",
                "KI",
                "MH",
                "FM",
                "NR",
                "NC",
                "NZ",
                "NU",
                "NF",
                "MP",
                "PW",
                "PG",
                "PN",
                "WS",
                "SB",
                "TK",
                "TO",
                "TV",
                "UM",
                "VU",
                "WF"
            ]
        ],
        [
            'region' => "Americas",
            'countries' => [
                "AI",
                "AG",
                "AR",
                "AW",
                "BS",
                "BB",
                "BZ",
                "BM",
                "BO",
                "BQ",
                "BV",
                "BR",
                "CA",
                "KY",
                "CL",
                "CO",
                "CR",
                "CU",
                "CW",
                "DM",
                "DO",
                "EC",
                "SV",
                "FK",
                "GF",
                "GL",
                "GD",
                "GP",
                "GT",
                "GY",
                "HT",
                "HN",
                "JM",
                "MQ",
                "MX",
                "MS",
                "NI",
                "PA",
                "PY",
                "PE",
                "PR",
                "BL",
                "KN",
                "LC",
                "MF",
                "PM",
                "VC",
                "SX",
                "GS",
                "SR",
                "TT",
                "TC",
                "US",
                "UY",
                "VE",
                "VG",
                "VI"
            ]
        ]
    ];
  }

}
