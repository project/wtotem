<?php

namespace Drupal\wtotem\lib\Common;

/**
 * WebTotem App class.
 */
class App {
  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'wtotem.settings';

  /**
   * Returns a mutable configuration object for a given name.
   *
   * @return \Drupal\Core\Config\Config
   *   A configuration object.
   */
  protected static function config() {
    return \Drupal::configFactory()->getEditable(static::SETTINGS);
  }

  /**
   * Get config option.
   *
   * @param string $option
   *   Option name.
   *
   * @return mixed
   *   Returns saved data by option name.
   */
  public static function getOption($option) {
    return self::config()->get('wtotem_' . $option);
  }

  /**
   * Save multiple configuration options.
   *
   * @param array $options
   *   Array of data, key is name of option.
   *
   * @return bool
   *   Returns TRUE after setting the options.
   */
  public static function setOptions(array $options) {

    $config = self::config();
    foreach ($options as $option => $value) {
      $config->set('wtotem_' . $option, $value);
    }
    $config->save();

    return TRUE;
  }

  /**
   * Clear multiple configuration options.
   *
   * @param array $options
   *   Array of data, key is name of option.
   *
   * @return bool
   *   Returns TRUE after clearing the options.
   */
  public static function clearOptions(array $options) {

    $config = self::config();
    foreach ($options as $option) {
      $config->clear('wtotem_' . $option);
    }
    $config->save();

    return TRUE;
  }

  /**
   * Set host data.
   *
   * @return void
   */
  public static function setHost($host_name, $host_id) {
    self::setOptions([
      'host_id' => $host_id,
      'host_name' => $host_name,
    ]);
  }

  /**
   * Save multiple some options to session.
   *
   * @param array $options
   *   Array of data, key is name of option.
   *
   * @return bool
   *   Returns TRUE after setting the session options.
   */
  public static function setSessionOptions(array $options) {
    $session = \Drupal::request()->getSession();

    foreach ($options as $option => $value) {
      $session->set('wtotem.' . $option, $value);
    }

    return TRUE;
  }

  /**
   * Get option from session.
   *
   * @param string $option
   *   Option name.
   *
   * @return mixed
   *   Returns saved data by option name.
   */
  public static function getSessionOption($option) {
    $session = \Drupal::request()->getSession();
    return $session->get('wtotem.' . $option);
  }

  /**
   * Save authentication token and token expiration dates in settings.
   *
   * @param array $params
   *   Parameters for authorization.
   *
   * @return string
   *   Returns TRUE after setting the options.
   */
  public static function login(array $params) {
    $token_expired = time() + $params['token']['expiresIn'] - 60;

    self::setOptions([
      'activated' => TRUE,
      'auth_token_expired' => $token_expired,
      'auth_token' => $params['token']['value'],
      'api_key' => $params['api_key'],
      'host_id' => '',
      'host_name' => '',
    ]);

    return TRUE;
  }

  /**
   * Checks whether the user has activated the module using the API key.
   *
   * @return bool
   *   Returns the module activation status.
   */
  public static function activated() {
    return (boolean) self::getOption('activated');
  }

  /**
   * Remove module settings.
   *
   * @return string
   *   Returns TRUE after clearing the options.
   */
  public static function logout() {

    self::clearOptions([
      'activated',
      'auth_token_expired',
      'auth_token',
      'api_key',
      'api_url',
      'host_id',
      'host_name',
      'am_installed',
    ]);
    return TRUE;
  }

  /**
   * Set notification.
   *
   * @param string $type
   *   Notification Type.
   * @param string $notice
   *   Notification Text.
   */
  public static function setNotification($type, $notice) {
    $notifications = self::getSessionOption('notifications') ?: [];

    if (array_key_exists($type, $notifications)) {
      if (!in_array($notice, $notifications[$type])) {
        $notifications[$type][] = $notice;
        self::setSessionOptions(['notifications' => $notifications]);
      }
    }
    else {
      $notifications[$type][] = $notice;
      self::setSessionOptions(['notifications' => $notifications]);
    }

  }

  /**
   * Get notifications.
   *
   * @return array
   *   Notifications array.
   */
  public static function getNotifications() {
    $types = ['error', 'info', 'warning', 'success'];

    $notifications = self::getSessionOption('notifications') ?: [];

    foreach ($types as $type) {
      if (array_key_exists($type, $notifications)) {
        foreach ($notifications[$type] as $notification) {
          $result[] = ['type' => $type, 'notice' => $notification];
        }
      }
    }

    // Remove notifications.
    self::setSessionOptions(['notifications' => NULL]);

    return $result;
  }

}
