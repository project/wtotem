<?php

namespace Drupal\wtotem\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormState;

use Drupal\wtotem\lib\Common\API as WtotemAPI;
use Drupal\wtotem\lib\Common\App;
use Drupal\wtotem\lib\Common\Helper;
use Drupal\wtotem\lib\Common\AgentManager;
use Drupal\wtotem\lib\Common\Cache;

/**
 * Returns responses for WTotem routes.
 */
class WTotemController extends ControllerBase {

  /**
   * Dashboard page.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   Returns a render array.
   *
   * @throws \Drupal\Core\Form\EnforcedResponseException
   * @throws \Drupal\Core\Form\FormAjaxException
   */
  public function dashboard() {

    // Get data from WebTotem API.
    $host = WtotemAPI::siteInfo();

    if($cacheData = Cache::getdata('getAllData', $host['id'])){
      $data = $cacheData['data'];
    } else {
      $data = WtotemAPI::getAllData($host['id']);
      Cache::setData(['getAllData' => $data], $host['id']);
    }

    // Reset session data.
    App::setSessionOptions([
      'firewall_period' => NULL,
      'ram_period' => NULL,
      'cpu_period' => NULL,
    ]);

    // Start build array for rendering.
    // Header with menu.
    $build[] = [
      "#theme_mode" => Helper::getThemeMode(),
        "#notifications" => Helper::getNotifications(),
      "#is_active" => ['dashboard' => 'wtotem_nav__link_active'],
      "#created_at" => $data['agentManager']['createdAt'],

      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
          'wtotem/wtotem.main_js',
          'wtotem/wtotem.header',
        ],
      ],
      '#theme' => 'wtotem_header',
    ];

    // Port scanner form.
    $form_state = new FormState();
    $build[] = $this->formBuilder()->buildForm('Drupal\wtotem\Form\WTotemPortsForm', $form_state);

    // Scoring block.
    $service_data = $data['scoring']['result'];
    $total_score = round($data['scoring']['score']);
    $score_grading = Helper::scoreGrading($total_score);
    $build[] = [
      "#host_id"     => $host['id'],
      "#total_score"   => $total_score . "%",
      "#tested_on"     => $data['scoring']['lastTest']['time'],
      "#server_ip"     => $service_data['ip'] ?: ' - ',
      "#location"    => Helper::getCountryName($service_data['country']) ?: ' - ',
      "#is_higher_than"  => $service_data['isHigherThan'] . '%',
      "#grade"       => $score_grading['grade'],
      "#color"       => $score_grading['color'],
      '#attached' => [
        'library' => [
          'wtotem/wtotem.dashboard',
        ],
      ],
      '#theme' => 'wtotem_score',
    ];

    // Agents installing process.
    $agents_data = [
      'av' => $data['antivirus']['status'],
      'waf'  => $data['firewall']['status'],
    ];

    $agents_statuses = Helper::getAgentsStatuses($agents_data);

    if (!$agents_statuses['option_statuses']['av'] or !$agents_statuses['option_statuses']['waf']) {
      $build[] = [
        "#process_status" => $agents_statuses['process_statuses'],
        '#attached' => [
          'library' => [
            'wtotem/wtotem.agents_install',
          ],
        ],
        '#theme' => 'wtotem_agents',
      ];
    }

    // Firewall header.
    $build[] = [
      "#title" => $this->t('Firewall activity'),
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_section_header',
    ];

    // Firewall stats.
    $service_data = $data['firewall'];
    $chart = Helper::generateWafChart($service_data['chart']);
    $build[] = [
      "#is_waf_training" => Helper::isWafTraining($data['agentManager']['createdAt']),
      "#most_attacks"  => Helper::getMostAttacksData($service_data['map']),
      "#all_attacks"   => $chart['count_attacks'],
      "#blocking"    => $chart['count_blocks'],
      "#not_blocking"  => $chart['count_attacks'] - $chart['count_blocks'],
      '#attached' => [
        'library' => [
          'wtotem/wtotem.firewall',
        ],
      ],
      '#theme' => 'wtotem_firewall_stats',
    ];

    // Firewall filter form.
    $form_state = new FormState();
    $build[] = $this->formBuilder()->buildForm('Drupal\wtotem\Form\WTotemWafFilterForm', $form_state);

    // Firewall blocks.
    $build[] = [
      "#chart" => $chart['chart'],
      "#logs"  => Helper::wafLogs($service_data['logs']['edges']),
      '#attached' => [
        'library' => [
          'wtotem/wtotem.firewall',
        ],
      ],
      '#theme' => 'wtotem_firewall',
    ];

    // Server Status header.
    $build[] = [
      "#title" => $this->t('Server resources'),
      "#tooltip" => [
        'title' => $this->t('Server resources'),
        'test' => $this->t('Displays critical data about web-server usage.
        A large load on a server can slow down the website performance.'),
      ],
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_section_header',
    ];

    // Server Status RAM.
    $service_data = $data['serverStatus'];
    $build[] = [
      "#info" => $service_data['info'],
      "#ram_chart" => Helper::generateChart($service_data['ramChart']),

      '#attached' => [
        'library' => [
          'wtotem/wtotem.dashboard',
        ],
      ],
      '#theme' => 'wtotem_server_status_ram',
    ];

    // Server Status CPU.
    $build[] = [
      "#cpu_chart" => Helper::generateChart($service_data['cpuChart']),

      '#attached' => [
        'library' => [
          'wtotem/wtotem.dashboard',
        ],
      ],
      '#theme' => 'wtotem_server_status_cpu',
    ];

    // Antivirus header.
    $build[] = [
      "#title" => $this->t('Antivirus'),
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_section_header',
    ];

    // Antivirus stats blocks.
    $antivirus_stats = $data['antivirus']['stats'];
    $build[] = [
      "#changes"  => $antivirus_stats['changed'] ?: 0,
      "#scanned"  => $antivirus_stats['scanned'] ?: 0,
      "#deleted"  => $antivirus_stats['deleted'] ?: 0,
      "#infected" => $antivirus_stats["infected"] ?: 0,
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_antivirus_stats',
    ];

    // Monitoring header.
    $build[] = [
      "#title" => $this->t('Monitoring'),
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_section_header',
    ];

    // Monitoring blocks.
    $ssl= false;
    if($data['sslResults']['results']){
      $ssl = [
        'status' => Helper::getStatusData($data['sslResults']['results'][0]['certStatus']),
        'cert_name' => $data['sslResults']['results'][0]['certIssuerName'],
        'days_left' => Helper::daysLeft($data['sslResults']['results'][0]['certExpiryDate']),
        'issue_date' => Helper::dateFormatter($data['sslResults']['results'][0]['certIssueDate']),
        'expiry_date' => Helper::dateFormatter($data['sslResults']['results'][0]['certExpiryDate']),
      ];
    }

    $domain = false;
    if (Helper::isKz()) {
      $domain = [
        'status' => Helper::getStatusData($data['domain']['lastScanResult']['status']),
        "redirect_link" => $data['domain']['lastScanResult']['redirectLink'],
        "is_created_at" => (bool)$data['domain']['lastScanResult']['time'],
        "created_at" => Helper::dateFormatter($data['domain']['lastScanResult']['time']),
        "is_taken" => $data['domain']['lastScanResult']['isTaken'],
        "ips" => $data['domain']['lastScanResult']['ips'],
        "protection" => $data['domain']['lastScanResult']['protection'],
      ];
    }

    $build[] = [
      "#ssl" => $ssl,
      "#domain"  => $domain,
      '#reputation' => [
        "status" => Helper::getStatusData($data['reputation']['status']),
        "blacklists_entries" => Helper::blacklistsEntries(
          $data['reputation']['status'],
          $data['reputation']['virusList']),
        "info" => Helper::getReputationInfo($data['reputation']['status']),
        "last_test" => Helper::dateFormatter($data['reputation']['lastTest']['time']),
      ],

      '#attached' => [
        'library' => [
          'wtotem/wtotem.dashboard',
        ],
      ],
      '#theme' => 'wtotem_monitoring',
    ];

    // Scanning header.
    $build[] = [
      "#title" => $this->t('Scanning'),
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_section_header',
    ];

    // Scanning blocks.
    $build[] = [
      "#ports"  => [
        'status' => Helper::getStatusData($data['ports']['status']),
        "TCPResults" => Helper::getOpenPortsData($data['ports']['TCPResults']),
        "ignore_ports" => $data['ports']['ignorePorts'],
        "last_test" => Helper::dateFormatter($data['ports']['lastTest']['time']),
      ],
      "#open_path" => [
        'status' => Helper::getStatusData(($data['openPathSearch']['paths']) ? 'warning' : 'clean'),
        "last_test" => Helper::dateFormatter($data['openPathSearch']['time']),
        "paths" => $data['openPathSearch']['paths'],
      ],

      '#attached' => [
        'library' => [
          'wtotem/wtotem.dashboard',
        ],
      ],
      '#theme' => 'wtotem_scanning',
    ];

    // Footer.
    $build[] = [
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_footer',
    ];


    return $build;
  }


  /**
   * Open paths page.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   Returns a render array.
   *
   * @throws \Drupal\Core\Form\EnforcedResponseException
   * @throws \Drupal\Core\Form\FormAjaxException
   */
  public function open_paths() {

    // Get data from WebTotem API.
    $host = WtotemAPI::siteInfo();

    if($cacheData = Cache::getdata('getOpenPaths', $host['id'])){
      $data = $cacheData['data'];
    } else {
      $data = WtotemAPI::getOpenPaths($host['id']);
      Cache::setData(['getOpenPaths' => $data], $host['id'], 1);
    }

    // Header with menu.
    $build[] = [
      "#theme_mode" => Helper::getThemeMode(),
      "#notifications" => Helper::getNotifications(),
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
          'wtotem/wtotem.main_js',
          'wtotem/wtotem.header',

        ],
      ],
      '#theme' => 'wtotem_header',
    ];


    // Start build array for rendering.
    // Open paths.

    $build[] = [
      "#paths" => $data['paths'],
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_open_paths_page',
    ];

    // Footer.
    $build[] = [
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_footer',
    ];

    return $build;
  }

  /**
   * Firewall page.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   Returns a render array.
   *
   * @throws \Drupal\Core\Form\EnforcedResponseException
   * @throws \Drupal\Core\Form\FormAjaxException
   */
  public function firewall() {

    // Get data from WebTotem API.
    $host = WtotemAPI::siteInfo();

    if($cacheData = Cache::getdata('getFirewall', $host['id'])){
      $data = $cacheData['data'];
    } else {
      $data = WtotemAPI::getFirewall($host['id'], 10, NULL, 7);
      Cache::setData(['getFirewall' => $data], $host['id'], 1);
    }

    $service_data = $data['firewall'];

    // Reset session data.
    App::setSessionOptions([
      'firewall_period' => NULL,
      'firewall_cursor' => $service_data['logs']['pageInfo']['endCursor'],
    ]);

    // Header with menu.
    $build[] = [
      "#theme_mode" => Helper::getThemeMode(),
      "#notifications" => Helper::getNotifications(),
      "#is_active" => ['firewall' => 'wtotem_nav__link_active'],
      "#created_at" => $data['agentManager']['createdAt'],
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
          'wtotem/wtotem.main_js',
          'wtotem/wtotem.header',

        ],
      ],
      '#theme' => 'wtotem_header',
    ];

    // Start build array for rendering.
    // Firewall header.
    $build[] = [
      "#title" => $this->t('Firewall activity'),
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_section_header',
    ];

    // Attacks map blocks.
    $world_map_json = WTOTEM_MODULE_PATH . '/js/world_map.json';
    $build[] = [
      "#attacks_map" => Helper::generateAttacksMapChart($service_data['map']),
      "#world_map_json" => $world_map_json,
      '#attached' => [
        'library' => [
          'wtotem/wtotem.firewall',
        ],
      ],
      '#theme' => 'wtotem_attacks_map',
    ];

    // Firewall stats.
    $chart = Helper::generateWafChart($service_data['chart']);
    $build[] = [
      "#is_waf_training" => Helper::isWafTraining($data['agentManager']['createdAt']),
      "#all_attacks"   => $chart['count_attacks'],
      "#blocking"    => $chart['count_blocks'],
      "#not_blocking"  => $chart['count_attacks'] - $chart['count_blocks'],
      "#most_attacks"  => Helper::getMostAttacksData($service_data['map']),
      '#attached' => [
        'library' => [
          'wtotem/wtotem.firewall',
        ],
      ],
      '#theme' => 'wtotem_firewall_stats',
    ];

    // Firewall filter form.
    $form_state = new FormState();
    $build[] = $this->formBuilder()->buildForm('Drupal\wtotem\Form\WTotemWafFilterForm', $form_state);

    // Firewall blocks.
    $build[] = [
      "#chart" => $chart['chart'],
      "#logs"  => Helper::wafLogs($service_data['logs']['edges']),
      '#has_next_page' => $service_data['logs']['pageInfo']['hasNextPage'],
      '#page'  => 'firewall',
      '#attached' => [
        'library' => [
          'wtotem/wtotem.firewall',
        ],
      ],
      '#theme' => 'wtotem_firewall',
    ];

    // Footer.
    $build[] = [
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_footer',
    ];

    return $build;
  }

  /**
   * Antivirus page.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   Returns a render array.
   *
   * @throws \Drupal\Core\Form\EnforcedResponseException
   * @throws \Drupal\Core\Form\FormAjaxException
   */
  public function antivirus() {

    // Reset session data.
    App::setSessionOptions([
      'antivirus_cursor' => NULL,
      'antivirus_event' => NULL,
      'antivirus_permissions' => NULL,
    ]);

    // Header with menu.
    $build[] = [
      "#theme_mode" => Helper::getThemeMode(),
      "#notifications" => Helper::getNotifications(),
      "#is_active" => ['antivirus' => 'wtotem_nav__link_active'],
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
          'wtotem/wtotem.main_js',
          'wtotem/wtotem.header',
        ],
      ],
      '#theme' => 'wtotem_header',
    ];

    // Get data from WebTotem API.
    $host = WtotemAPI::siteInfo();

    $params = [
      'host_id' => $host['id'],
      'limit' => 10,
      'cursor' => NULL,
      'days' => 365,
      'event' => FALSE,
      'permissions' => FALSE,
    ];

    if($cacheData = Cache::getdata('getAntivirus', $host['id'])){
      $data = $cacheData['data'];
    } else {
      $data = WtotemAPI::getAntivirus($params);
      Cache::setData(['getAntivirus' => $data], $host['id']);
    }

    App::setSessionOptions([
      'antivirus_cursor' => $data['log']['pageInfo']['endCursor'],
    ]);

    // Antivirus header.
    $build[] = [
      "#title" => $this->t('Antivirus'),
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_section_header',
    ];

    // Antivirus stats blocks.
    $stats = $data['stats'];
    $build[] = [
      "#changes"  => $stats['changed'] ?: 0,
      "#scanned"  => $stats['scanned'] ?: 0,
      "#deleted"  => $stats['deleted'] ?: 0,
      "#infected" => $stats["infected"] ?: 0,
      "#page" => 'antivirus',
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_antivirus_stats',
    ];

    // Quarantine logs blocks.
    $quarantine_logs = $data['quarantine'];
    $quarantine_count = count($quarantine_logs);

    $build[] = [
      "#logs"  => Helper::getQuarantineLogs($quarantine_logs),
      "#count"  => $quarantine_count,
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_quarantine',
    ];

    // Antivirus filter form.
    $form_state = new FormState();
    $build[] = $this->formBuilder()->buildForm('Drupal\wtotem\Form\WTotemAVFilterForm', $form_state);

    // Antivirus blocks.
    $build[] = [
      "#logs" => Helper::getAntivirusLogs($data['log']['edges']),
      "#has_next_page" => $data['log']['pageInfo']['hasNextPage'],
      '#last_scan' => Helper::dateFormatter($data['lastTest']['time']),
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_antivirus',
    ];

    // Footer.
    $build[] = [
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_footer',
    ];

    return $build;
  }

  /**
   * Reports page.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   Returns a render array.
   *
   * @throws \Drupal\Core\Form\EnforcedResponseException
   * @throws \Drupal\Core\Form\FormAjaxException
   */
  public function reports() {

    // Get data from WebTotem API.
    $host = WtotemAPI::siteInfo();

    if($cacheData = Cache::getdata('getAllReports', $host['id'])){
      $data = $cacheData['data'];
    } else {
      $data = WtotemAPI::getAllReports($host['id']);
      Cache::setData(['getAllReports' => $data], $host['id']);
    }

    App::setSessionOptions([
      'reports_cursor' => $data['pageInfo']['endCursor'],
      'reports_m_cursor' => $data['pageInfo']['endCursor'],
    ]);

    // Header with menu.
    $build[] = [
      "#theme_mode" => Helper::getThemeMode(),
      "#notifications" => Helper::getNotifications(),
      "#is_active" => ['reports' => 'wtotem_nav__link_active'],
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
          'wtotem/wtotem.main_js',
          'wtotem/wtotem.header',
        ],
      ],
      '#theme' => 'wtotem_header',
    ];

    // Reports form.
    $form_state = new FormState();
    $build[] = $this->formBuilder()->buildForm('Drupal\wtotem\Form\WTotemReportsForm', $form_state);

    // Reports.
    $build[] = [
      "#reports" => Helper::getReports($data['edges']),
      "#has_next_page" => $data['pageInfo']['hasNextPage'],
      '#attached' => [
        'library' => [
          'wtotem/wtotem.reports',
        ],
      ],
      '#theme' => 'wtotem_reports',
    ];

    // Footer.
    $build[] = [
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_footer',
    ];

    return $build;
  }

  /**
   * Information page.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   Returns a render array.
   *
   * @throws \Drupal\Core\Form\EnforcedResponseException
   * @throws \Drupal\Core\Form\FormAjaxException
   */
  public function information() {

    // Header with menu.
    $build[] = [
      "#theme_mode" => Helper::getThemeMode(),
      "#notifications" => Helper::getNotifications(),
      "#is_active" => ['information' => 'wtotem_nav__link_active'],
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
          'wtotem/wtotem.main_js',
          'wtotem/wtotem.header',
        ],
      ],
      '#theme' => 'wtotem_header',
    ];

    $build[] = [
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_information',
    ];

    // Footer.
    $build[] = [
      '#attached' => [
        'library' => [
          'wtotem/wtotem.main_css',
        ],
      ],
      '#theme' => 'wtotem_footer',
    ];

    return $build;
  }

  /**
   * Checks whether the user has activated the module using the API key.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {

    // Checking whether agents are installed, if they are not installed, then install.
    AgentManager::checkAgents();

    return AccessResult::allowedIf($account->hasPermission('administer wtotem') && App::activated());
  }

}
