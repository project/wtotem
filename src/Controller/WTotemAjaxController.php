<?php

namespace Drupal\wtotem\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;

use Drupal\wtotem\lib\Common\AgentManager;
use Drupal\wtotem\lib\Common\API as WtotemAPI;
use Drupal\wtotem\lib\Common\App;
use Drupal\wtotem\lib\Common\Helper;
use Drupal\wtotem\lib\Common\Cache;

/**
 * Returns ajax responses.
 */
class WTotemAjaxController extends ControllerBase {

  /**
   * Request to update charts with parameters.
   *
   * @param string $name
   *   Service name.
   * @param int|array $days
   *   Requested period.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxChart($name, $days = 7) {

    // New response.
    $ajax_response = new AjaxResponse();

    $days = (integer) $days;
    $service = NULL;
    $build = [];
    $host = WtotemAPI::siteInfo();

    switch ($name) {
      case 'waf':

        App::setSessionOptions(['firewall_period' => $days]);
        // Firewall chart.
        $data = WtotemAPI::getFirewallChart($host['id'], $days);
        $chart = Helper::generateWafChart($data['chart']);

        $build[] = [
          "#days" => $days,
          "#chart" => $chart['chart'],
          '#theme' => 'wtotem_firewall_chart',
        ];

        // Firewall logs.
        $data = WtotemAPI::getFirewall($host['id'], 10, NULL, $days);
        $firewall = $data['firewall'];

        $waf_logs[] = [
          "#logs" => Helper::wafLogs($firewall['logs']['edges']),
          '#theme' => 'wtotem_firewall_logs',
        ];

        // Firewall stats.
        $waf_stats[] = [
          "#is_waf_training" => Helper::isWafTraining($data['agentManager']['createdAt']),
          "#all_attacks" => $chart['count_attacks'],
          "#blocking" => $chart['count_blocks'],
          "#not_blocking" => $chart['count_attacks'] - $chart['count_blocks'],
          "#most_attacks" => Helper::getMostAttacksData($firewall['map']),

          '#theme' => 'wtotem_firewall_stats',
        ];

        App::setSessionOptions([
          'firewall_cursor' => $firewall['logs']['pageInfo']['endCursor'],
        ]);

        $has_next_page = $firewall['logs']['pageInfo']['hasNextPage'];

        if (!$has_next_page) {
          $ajax_response->addCommand(new InvokeCommand('#waf_load_more', 'remove'));
        }
        else {
          $link = '<a href="/wtotem/lazy_load/firewall" class="use-ajax wtotem_more_btn" id="waf_load_more">' . $this->t('Show more') . '</a>';
          $ajax_response->addCommand(new HtmlCommand('.wtotem_more_btn_wrap', $link, $settings = NULL));
        }

        $ajax_response->addCommand(new HtmlCommand('#waf_logs_wrap', $waf_logs, $settings = NULL));
        $ajax_response->addCommand(new ReplaceCommand('#firewall_stats', $waf_stats, $settings = NULL));

        $service = 'waf';
        break;

      case 'cpu':
        App::setSessionOptions(['cpu_period' => $days]);

        $data = WtotemAPI::getServerStatusData($host['id'], $days);
        $chart = Helper::generateChart($data['cpuChart'], $days);

        $build[] = [
          "#days" => $days,
          "#chart" => $chart,
          '#theme' => 'wtotem_cpu_chart',
        ];

        $service = 'cpu';
        break;

      case 'ram':
        App::setSessionOptions(['ram_period' => $days]);

        $data = WtotemAPI::getServerStatusData($host['id'], $days);
        $chart = Helper::generateChart($data['ramChart'], $days);

        $build[] = [
          "#days" => $days,
          "#chart" => $chart,
          '#theme' => 'wtotem_ram_chart',
        ];

        $service = 'ram';
        break;

      case 'map':
        $data = WtotemAPI::getFirewallChart($host['id'], $days);
        $chart = Helper::generateAttacksMapChart($data['map']);
        $world_map_json = WTOTEM_MODULE_PATH . '/js/world_map.json';

        $build[] = [
          "#attacks_map" => $chart,
          "#world_map_json" => $world_map_json,
          '#theme' => 'wtotem_map_chart',
        ];

        $service = 'map';
        break;

    }

    if ($service) {
      // Set vars.
      $wrap = '#' . $service . '_chart_wrap';
      $active_selector = '#' . $service . '_days_' . $days;
      $all_items = '#' . $service . '_chart_btn a';
      $active_class = ['wtotem_chart-first__btn_active'];

      // Commands Ajax.
      $ajax_response->addCommand(new HtmlCommand($wrap, $build, $settings = NULL));
      $ajax_response->addCommand(new InvokeCommand($all_items, 'removeClass', $active_class));
      $ajax_response->addCommand(new InvokeCommand($active_selector, 'addClass', $active_class));

    }

    self::notifications($ajax_response);
    // Return response.
    return $ajax_response;

  }

  /**
   * Data lazy load.
   *
   * @param string $name
   *   Service name.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function lazyLoad($name) {

    // New response.
    $ajax_response = new AjaxResponse();

    $service = NULL;
    $host = WtotemAPI::siteInfo();

    switch ($name) {
      case 'firewall':
        $cursor = App::getSessionOption('firewall_cursor') ?: NULL;
        $period = App::getSessionOption('firewall_period') ?: 365;

        $data = WtotemAPI::getFirewall($host['id'], 10, $cursor, $period);
        $service_data = $data['firewall'];
        $has_next_page = $service_data['logs']['pageInfo']['hasNextPage'];

        App::setSessionOptions([
          'firewall_cursor' => $service_data['logs']['pageInfo']['endCursor'],
        ]);

        $build[] = [
          "#logs" => Helper::wafLogs($service_data['logs']['edges']),
          '#theme' => 'wtotem_firewall_logs',
        ];

        $service = 'waf';
        break;

      case 'antivirus':
        $cursor = App::getSessionOption('antivirus_cursor') ?: NULL;
        $event = App::getSessionOption('antivirus_event') ?: NULL;
        $permissions = App::getSessionOption('antivirus_permissions') ?: NULL;

        $params = [
          'host_id' => $host['id'],
          'limit' => 10,
          'days' => 365,
          'cursor' => $cursor,
          'event' => $event,
          'permissions' => $permissions,
        ];

        $data = WtotemAPI::getAntivirus($params);
        $has_next_page = $data['log']['pageInfo']['hasNextPage'];

        App::setSessionOptions([
          'antivirus_cursor' => $data['log']['pageInfo']['endCursor'],
        ]);

        // Antivirus logs.
        $build[] = [
          "#logs" => Helper::getAntivirusLogs($data['log']['edges']),
          '#theme' => 'wtotem_antivirus_logs',
        ];

        $service = 'av';
        break;

      case 'reports':
        $cursor = App::getSessionOption('reports_cursor') ?: NULL;

        $data = WtotemAPI::getAllReports($host['id'], 10, $cursor);
        $has_next_page = $data['pageInfo']['hasNextPage'];

        App::setSessionOptions([
          'reports_cursor' => $data['pageInfo']['endCursor'],
        ]);

        // Reports.
        $build[] = [
          "#reports" => Helper::getReports($data['edges']),
          "#has_next_page" => $data['pageInfo']['hasNextPage'],
          '#theme' => 'wtotem_reports_list',
        ];

        $service = 'reports';
        break;

      case 'reports_m':
        $cursor = App::getSessionOption('reports_m_cursor') ?: NULL;

        $data = WtotemAPI::getAllReports($host['id'], 10, $cursor);
        $has_next_page = $data['pageInfo']['hasNextPage'];

        App::setSessionOptions([
          'reports_m_cursor' => $data['pageInfo']['endCursor'],
        ]);

        // Reports mobile.
        $build[] = [
          "#reports" => Helper::getReports($data['edges']),
          "#has_next_page" => $data['pageInfo']['hasNextPage'],
          '#theme' => 'wtotem_reports_list_mobile',
        ];

        $service = 'reports_m';
        break;

    }

    if ($service) {
      // Set vars.
      $wrap = '#' . $service . '_logs_wrap';
      $more_btn = '#' . $service . '_load_more';

      // Commands Ajax.
      if (!$has_next_page) {
        $ajax_response->addCommand(new InvokeCommand($more_btn, 'remove'));
      }
      $ajax_response->addCommand(new AppendCommand($wrap, $build));
    }

    self::notifications($ajax_response);
    // Return response.
    return $ajax_response;
  }

  /**
   * Request to add or remove a port to the ignore list.
   *
   * @param string $action
   *   Action add or remove.
   * @param int $port
   *   Port by the user.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ignorePorts($action, $port) {

    // New response.
    $ajax_response = new AjaxResponse();

    $host = WtotemAPI::siteInfo();

    switch ($action) {
      case 'add':
        $response = WtotemAPI::addIgnorePort($host['id'], $port);
        break;

      case 'remove':
        $response = WtotemAPI::removeIgnorePort($host['id'], $port);
        break;
    }

    if (!isset($response['errors'])) {

      $ports = WtotemAPI::getAllPortsList($host['id']);
      Cache::setData(['getAllPortsList' => $ports], $host['id']);

      $open_ports[] = [
        "#ports" => Helper::getOpenPortsData($ports['TCPResults']),
        '#theme' => 'wtotem_open_ports',
      ];

      $open_ports_few[] = [
        "#more" => true,
        "#ports" => $ports['TCPResults'] ? Helper::getOpenPortsData(array_slice($ports['TCPResults'], 0, 3)) : [],
        '#theme' => 'wtotem_open_ports',
      ];

      $ignore_ports[] = [
        "#ports" => $ports,
        '#theme' => 'wtotem_ignore_ports',
      ];

      $status = Helper::getStatusData($ports['status']);

      // Commands Ajax.
      $ajax_response->addCommand(new HtmlCommand('#open_ports_wrap', $open_ports_few ?? ''));
      $ajax_response->addCommand(new HtmlCommand('#port_result_list', $open_ports ?? ''));
      $ajax_response->addCommand(new HtmlCommand('#ignore_ports_wrap', $ignore_ports));
      $ajax_response->addCommand(new HtmlCommand('#port_status', $status['text']));
      $ajax_response->addCommand(new InvokeCommand('#port_status', 'addClass', [$status['class']]));
      $ajax_response->addCommand(new HtmlCommand('#port_last_check', Helper::dateFormatter($ports['lastTest']['time'])));
    }

    self::notifications($ajax_response);
    // Return response.
    return $ajax_response;
  }

  /**
   * Request to add a file to quarantine.
   *
   * @param string $action
   *   Add or remove file.
   * @param string $param
   *   File id or url.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function quarantine($action, $param) {

    // New response.
    $ajax_response = new AjaxResponse();

    $host = WtotemAPI::siteInfo();

    switch ($action) {
      case 'add':
        $param = str_replace("*1", "%", $param);
        $param = str_replace("*2", "/", $param);
        $response = WtotemAPI::moveToQuarantine($host['id'], $param);
        break;

      case 'remove':
        $response = WtotemAPI::moveFromQuarantine($param);
        break;
    }

    if (!isset($response['errors'])) {

      $quarantine_logs = WtotemAPI::getQuarantineList($host['id']);
      Cache::setData(['getQuarantineList' => $quarantine_logs], $host['id']);

      $quarantine_count = count($quarantine_logs);

      // Quarantine logs.
      $quarantine[] = [
        "#logs" => Helper::getQuarantineLogs($quarantine_logs),
        "#count" => $quarantine_count,
        '#theme' => 'wtotem_quarantine',
      ];

      // Commands Ajax.
      $ajax_response->addCommand(new ReplaceCommand('#wtotem_quarantine_wrap', $quarantine));

      $cursor = App::getSessionOption('antivirus_cursor') ?: NULL;
      $event = App::getSessionOption('antivirus_event') ?: NULL;
      $permissions = App::getSessionOption('antivirus_permissions') ?: NULL;

      $params = [
        'host_id' => $host['id'],
        'limit' => 10,
        'days' => 365,
        'cursor' => $cursor,
        'event' => $event,
        'permissions' => $permissions,
      ];

      $data = WtotemAPI::getAntivirus($params);
      $has_next_page = $data['log']['pageInfo']['hasNextPage'];

      App::setSessionOptions([
        'antivirus_cursor' => $data['log']['pageInfo']['endCursor'],
      ]);

      // Antivirus logs.
      $build[] = [
        "#logs" => Helper::getAntivirusLogs($data['log']['edges']),
        '#theme' => 'wtotem_antivirus_logs',
      ];

      // Commands Ajax.
      if (!$has_next_page) {
        $ajax_response->addCommand(new InvokeCommand('#av_load_more', 'remove'));
      }
      $ajax_response->addCommand(new HtmlCommand('#av_logs_wrap', $build));

    }

    self::notifications($ajax_response);
    // Return response.
    return $ajax_response;
  }

  /**
   * Request to restart re-scan and receive antivirus data.
   *
   * @param string $action
   *   Action to be performed.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function antivirus($action) {

    // New response.
    $ajax_response = new AjaxResponse();

    $host = WtotemAPI::siteInfo();

    switch ($action) {
      case 'rescan':
        $response = WtotemAPI::forceCheck($host['id'], 'av');

        if (!isset($response['errors'])) {
          $data = WtotemAPI::getAntivirusLastTest($host['id']);
          $last_scan = Helper::dateFormatter($data['lastTest']['time']);

          // Commands Ajax.
          $ajax_response->addCommand(new HtmlCommand('#wtotem_last_scan_av', $last_scan));
        }
        break;

      case 'download_report':
        $response = WtotemAPI::avExport($host['id']);
        if (!isset($response['errors'])) {
          $link = $response['data']['auth']['sites']['av']['export'];
          $ajax_response->addCommand(new RedirectCommand($link));
        }
        break;
    }

    self::notifications($ajax_response);
    // Return response.
    return $ajax_response;
  }

  /**
   * Filter files by status.
   *
   * @param string $file_status
   *   File status.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function antivirusFilter($file_status) {
    $ajax_response = new AjaxResponse();
    $statuses = ['scanned', 'modified', 'deleted', 'infected'];

    if (in_array($file_status, $statuses)) {
      $permission = App::getSessionOption('antivirus_permissions');
      $host = WtotemAPI::siteInfo();

      App::setSessionOptions([
        'antivirus_event' => $file_status,
      ]);

      $params = [
        'host_id' => $host['id'],
        'limit' => 10,
        'days' => 365,
        'cursor' => NULL,
        'event' => $file_status,
        'permissions' => $permission,
      ];

      $data = WtotemAPI::getAntivirus($params);
      $has_next_page = $data['log']['pageInfo']['hasNextPage'];

      App::setSessionOptions([
        'antivirus_cursor' => $data['log']['pageInfo']['endCursor'],
      ]);

      $build[] = [
        "#logs" => Helper::getAntivirusLogs($data['log']['edges']),
        '#theme' => 'wtotem_antivirus_logs',
      ];

      // Commands Ajax.
      if (!$has_next_page) {
        $ajax_response->addCommand(new InvokeCommand('#av_load_more', 'remove'));
      }
      else {
        $link = '<a href="/wtotem/lazy_load/antivirus" class="use-ajax wtotem_more_btn" id="av_load_more">' . $this->t('Show more') . '</a>';
        $ajax_response->addCommand(new HtmlCommand('.wtotem_more_btn_wrap', $link, $settings = NULL));
      }

      $ajax_response->addCommand(new HtmlCommand('#av_logs_wrap', $build));
    }

    return $ajax_response;
  }

  /**
   * Changing the theme mode.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function changeThemeMode() {

    // New response.
    $ajax_response = new AjaxResponse();

    $theme_mode = App::getSessionOption('theme_mode');
    $class = ['wtotem_theme—dark'];

    if ($theme_mode == 'dark') {
      App::setSessionOptions(['theme_mode' => 'light']);
      $ajax_response->addCommand(new InvokeCommand('.wtotem_body', 'removeClass', $class));
    }
    else {
      App::setSessionOptions(['theme_mode' => 'dark']);
      $ajax_response->addCommand(new InvokeCommand('.wtotem_body', 'addClass', $class));
    }

    // Return response.
    return $ajax_response;
  }

  /**
   * Set user time zone offset.
   *
   * @param string $offset
   *   Time zone offset from user browser.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function userTimeZone($offset) {
    // New response.
    $ajax_response = new AjaxResponse();

    $now = strtotime('now');
    $check = App::getOption('time_zone_check') ?: 0;

    // Checking whether an hour has elapsed since the previous request.
    if ($now >= $check) {
      $time_zone = WtotemAPI::getTimeZone();
      if ($time_zone) {
        $time_zone_offset = timezone_offset_get(new \DateTimeZone($time_zone), new \DateTime('now', new \DateTimeZone('Europe/London'))) / 3600;
        App::setOptions(['time_zone_check' => $now + 3600]);
      }
      else {
        $time_zone_offset = $offset;
      }

      App::setOptions(['time_zone_offset' => $time_zone_offset]);

    }

    // Return response.
    return $ajax_response;

  }

  /**
   * Request for a report link.
   *
   * @param string $action
   *   Action to be performed.
   * @param string $id
   *   Id assigned to the report.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function reports($action, $id) {

    // New response.
    $ajax_response = new AjaxResponse();

    switch ($action) {
      case 'download':
        $link = WtotemAPI::downloadReport($id);
        if ($link) {
          // Commands Ajax.
          $ajax_response->addCommand(new RedirectCommand($link));
        }
        break;
    }

    self::notifications($ajax_response);
    // Return response.
    return $ajax_response;
  }

  /**
   * Reinstall agents.
   *
   * First delete all agent files,
   * folders and records in the database, then install Agents.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function reinstallAgents() {

    // New response.
    $ajax_response = new AjaxResponse();

    if (AgentManager::removeAgents()) {
      AgentManager::amInstall();
    }

    $ajax_response->addCommand(new RedirectCommand('/admin/wtotem/dashboard'));

    self::notifications($ajax_response);
    // Return response.
    return $ajax_response;
  }

  /**
   * The process of installing agents (WAF, AV) on the main page.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function agentsInstallationProgress() {

    // New response.
    $ajax_response = new AjaxResponse();

    $av_installed = App::getOption('av_installed');
    $waf_installed = App::getOption('waf_installed');

    // Check if the agents are installed.
    if ($av_installed and $waf_installed) {
      $agents_statuses = [
        'process_statuses' => [
          'av' => 'installed',
          'waf' => 'installed',
        ],
      ];
    }
    else {
      // If not installed, then request statuses from the WebTotem API.
      $host = WtotemAPI::siteInfo();
      $data = WtotemAPI::getAgentsStatuses($host['id']);

      $agents_statuses = [
        'av' => $data['av']['status'],
        'waf' => $data['waf']['status'],
      ];

      $agents_statuses = Helper::getAgentsStatuses($agents_statuses);
    }

    $build[] = [
      "#process_status" => $agents_statuses['process_statuses'],
      '#attached' => [
        'library' => [
          'wtotem/wtotem.agents_install',
        ],
      ],
      '#theme' => 'wtotem_agents_installation',
    ];

    App::setOptions([
      'av_installed' => $agents_statuses['process_statuses']['av'] == 'installed' ? TRUE : FALSE,
      'waf_installed' => $agents_statuses['process_statuses']['waf'] == 'installed' ? TRUE : FALSE,
    ]);

    $ajax_response->addCommand(new HtmlCommand('#wtotem_agents_wrap', $build));

    self::notifications($ajax_response);

    // Return response.
    return $ajax_response;
  }

  /**
   * Creating a modal window.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public static function popup($action) {
    global $root_path;

    // New response.
    $ajax_response = new AjaxResponse();

    if ($action) {
      switch ($action) {
        case 'reinstall_agents':
          $build[] = [
            '#message' => sprintf(t('Some scanning data for %s may be deleted.'), $root_path),
            '#action' => 'reinstall_agents',
            '#theme' => 'wtotem_popup',
          ];
          break;

      }
      $ajax_response->addCommand(new AppendCommand('.wtotem_body', $build));

    }

    // Return response.
    return $ajax_response;
  }

  /**
   * Request to remove from the list of deny/allowed ip or url addresses.
   *
   * @param string $action
   *   Action to be performed.
   * @param string $id
   *   Id assigned to the ip or url address.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function remove($action, $id) {

    // New response.
    $ajax_response = new AjaxResponse();

    $host = WtotemAPI::siteInfo();

    switch ($action) {
      case 'ip_allow':
        $response = WtotemAPI::removeIpFromList($id);

        if ($response) {
          $data = WtotemAPI::getIpLists($host['id']);
          Cache::setData(['getIpLists' => $data], $host['id']);

          $build[] = [
            "#list" => Helper::getIpList($data['whiteList'], 'ip_allow'),
            '#theme' => 'wtotem_allow_deny_list',
          ];

          // Commands Ajax.
          $ajax_response->addCommand(new HtmlCommand('#wtotem_allow_list', $build));
        }
        break;

      case 'ip_deny':
        $response = WtotemAPI::removeIpFromList($id);

        if ($response) {
          $data = WtotemAPI::getIpLists($host['id']);
          Cache::setData(['getIpLists' => $data], $host['id']);

          $build[] = [
            "#list" => Helper::getIpList($data['blackList'], 'ip_deny'),
            '#theme' => 'wtotem_allow_deny_list',
          ];

          // Commands Ajax.
          $ajax_response->addCommand(new HtmlCommand('#wtotem_deny_list', $build));
        }
        break;

      case 'url_allow':
        $response = WtotemAPI::removeUrlFromAllowList($id);

        if ($response) {
          $data = WtotemAPI::getAllowUrlList($host['id']);
          Cache::setData(['getAllowUrlList' => $data], $host['id']);

          $build[] = [
            "#list" => Helper::getUrlAllowList($data),
            '#theme' => 'wtotem_allow_url_list',
          ];

          // Commands Ajax.
          $ajax_response->addCommand(new HtmlCommand('#wtotem_allow_url', $build));
        }
        break;

    }

    self::notifications($ajax_response);
    // Return response.
    return $ajax_response;
  }

  /**
   * Forced checking of services.
   *
   * @param string $service
   *   Action to be performed.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   *
   */
  public static function forceCheck($service) {
    // New response.
    $ajax_response = new AjaxResponse();

    $host = WtotemAPI::siteInfo();

    if($service){
      // force check service
      $_response = WtotemAPI::forceCheck($host['id'], $service);

      if (!isset($_response['errors'])) {

        switch ($service) {
          case 'ps':
            $ports = WtotemAPI::getAllPortsList($host['id']);

            if($ports['TCPResults']){
              $open_ports[] = [
                "#ports" => Helper::getOpenPortsData($ports['TCPResults']),
                '#theme' => 'wtotem_open_ports',
              ];

              $open_ports_few[] = [
                "#more" => true,
                "#ports" => $ports['TCPResults'] ? Helper::getOpenPortsData(array_slice($ports['TCPResults'], 0, 3)) : [],
                '#theme' => 'wtotem_open_ports',
              ];
            }

            $ignore_ports[] = [
              "#ports" => $ports,
              '#theme' => 'wtotem_ignore_ports',
            ];

            $status = Helper::getStatusData($ports['status']);

            // Commands Ajax.
            $ajax_response->addCommand(new HtmlCommand('#open_ports_wrap', $open_ports_few ?? ''));
            $ajax_response->addCommand(new HtmlCommand('#port_result_list', $open_ports ?? ''));
            $ajax_response->addCommand(new HtmlCommand('#ignore_ports_wrap', $ignore_ports));
            $ajax_response->addCommand(new HtmlCommand('#port_status', $status['text']));
            $ajax_response->addCommand(new InvokeCommand('#port_status', 'addClass', [$status['class']]));
            $ajax_response->addCommand(new HtmlCommand('#port_last_check', Helper::dateFormatter($ports['lastTest']['time'])));

            break;

          case 'ops':

            $open_path_data = WtotemAPI::getOpenPaths($host['id']);
            $open_path[] = [
              "#paths" => $open_path_data['paths'],
              '#theme' => 'wtotem_open_paths',
            ];
            $status = Helper::getStatusData(($open_path_data['paths']) ? 'warning' : 'clean');
            $ajax_response->addCommand(new HtmlCommand('#open_paths_wrap', $open_path));
            $ajax_response->addCommand(new HtmlCommand('#ops_status', $status['text']));
            $ajax_response->addCommand(new InvokeCommand('#ops_status', 'addClass', [$status['class']]));
            $ajax_response->addCommand(new HtmlCommand('#ops_last_check', Helper::dateFormatter($open_path_data['time'])));

            break;
        }

      }
    }


    self::notifications($ajax_response);
    // Return response.
    return $ajax_response;
  }

  /**
   * Updating the page data in the specified time interval.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function reloadPage($page) {
    // New response.
    $ajax_response = new AjaxResponse();

    // Get data from WebTotem API.
    $host = WtotemAPI::siteInfo();

    switch ($page) {
      case 'dashboard':

        $data = WtotemAPI::getAllData($host['id']);
        Cache::setData(['getAllData' => $data], $host['id']);

        // Start build array for rendering.
        // Scoring block.
        $service_data = $data['scoring']['result'];
        $total_score = round($data['scoring']['score']);
        $score_grading = Helper::scoreGrading($total_score);
        $build['scoring'] = [
          "#host_id" => $host['id'],
          "#total_score" => $total_score . "%",
          "#tested_on" => $data['scoring']['lastTest']['time'],
          "#server_ip" => $service_data['ip'] ?: ' - ',
          "#location" => Helper::getCountryName($service_data['country']) ?: ' - ',
          "#is_higher_than" => $service_data['isHigherThan'] . '%',
          "#grade" => $score_grading['grade'],
          "#color" => $score_grading['color'],
          '#attached' => [
            'library' => [
              'wtotem/wtotem.dashboard',
            ],
          ],
          '#theme' => 'wtotem_score',
        ];

        // Firewall stats.
        $period = App::getSessionOption('firewall_period') ?: 7;
        $service_data = $period ? WtotemAPI::getFirewall($host['id'], 10, NULL, $period) : $data;
        $service_data = $service_data['firewall'];

        $chart = Helper::generateWafChart($service_data['chart']);
        $build['firewall_stats'] = [
          "#is_waf_training" => Helper::isWafTraining($data['agentManager']['createdAt']),
          "#most_attacks" => Helper::getMostAttacksData($service_data['map']),
          "#all_attacks" => $chart['count_attacks'],
          "#blocking" => $chart['count_blocks'],
          "#not_blocking" => $chart['count_attacks'] - $chart['count_blocks'],
          '#attached' => [
            'library' => [
              'wtotem/wtotem.firewall',
            ],
          ],
          '#theme' => 'wtotem_firewall_stats',
        ];

        $build['chart_periods'] = [
          "#service" => 'waf',
          "#days" => is_array($period) ? 7 : $period,
          '#attached' => [
            'library' => [
              'wtotem/wtotem.main_css',
            ],
          ],
          '#theme' => 'wtotem_chart_periods',
        ];

        // Firewall blocks.
        $build['firewall_data'] = [
          "#chart" => $chart['chart'],
          "#days" => $period,
          "#logs" => Helper::wafLogs($service_data['logs']['edges']),
          '#attached' => [
            'library' => [
              'wtotem/wtotem.firewall',
            ],
          ],
          '#theme' => 'wtotem_firewall',
        ];

        // Server Status RAM.
        $period = App::getSessionOption('ram_period') ?: 7;
        $service_data = $period ? WtotemAPI::getServerStatusData($host['id'], $period) : $data['serverStatus'];

        $build['server_status_ram'] = [
          "#info" => $service_data['info'],
          "#ram_chart" => Helper::generateChart($service_data['ramChart']),
          "#days" => $period,

          '#attached' => [
            'library' => [
              'wtotem/wtotem.dashboard',
            ],
          ],
          '#theme' => 'wtotem_server_status_ram',
        ];

        // Server Status CPU.
        $period = App::getSessionOption('cpu_period') ?: 7;
        $service_data = $period ? WtotemAPI::getServerStatusData($host['id'], $period) : $data['serverStatus'];

        $build['server_status_cpu'] = [
          "#cpu_chart" => Helper::generateChart($service_data['cpuChart']),
          "#days" => $period,

          '#attached' => [
            'library' => [
              'wtotem/wtotem.dashboard',
            ],
          ],
          '#theme' => 'wtotem_server_status_cpu',
        ];

        // Antivirus stats blocks.
        $antivirus_stats = $data['antivirus']['stats'];
        $build['antivirus_stats'] = [
          "#changes" => $antivirus_stats['changed'] ?: 0,
          "#scanned" => $antivirus_stats['scanned'] ?: 0,
          "#deleted" => $antivirus_stats['deleted'] ?: 0,
          "#infected" => $antivirus_stats["infected"] ?: 0,
          '#attached' => [
            'library' => [
              'wtotem/wtotem.main_css',
            ],
          ],
          '#theme' => 'wtotem_antivirus_stats',
        ];

        // Monitoring blocks.
        $ssl= false;
        if($data['sslResults']['results']){
          $ssl = [
            'status' => Helper::getStatusData($data['sslResults']['results'][0]['certStatus']),
            'cert_name' => $data['sslResults']['results'][0]['certIssuerName'],
            'days_left' => Helper::daysLeft($data['sslResults']['results'][0]['certExpiryDate']),
            'issue_date' => Helper::dateFormatter($data['sslResults']['results'][0]['certIssueDate']),
            'expiry_date' => Helper::dateFormatter($data['sslResults']['results'][0]['certExpiryDate']),
          ];
        }

        $domain = false;
        if (Helper::isKz()) {
          $domain = [
            'status' => Helper::getStatusData($data['domain']['lastScanResult']['status']),
            "redirect_link" => $data['domain']['lastScanResult']['redirectLink'],
            "is_created_at" => (bool)$data['domain']['lastScanResult']['time'],
            "created_at" => Helper::dateFormatter($data['domain']['lastScanResult']['time']),
            "is_taken" => $data['domain']['lastScanResult']['isTaken'],
            "ips" => $data['domain']['lastScanResult']['ips'],
            "protection" => $data['domain']['lastScanResult']['protection'],
          ];
        }

        $build['monitoring'] = [
          "#ssl" => $ssl,
          "#domain"  => $domain,
          '#reputation' => [
            "status" => Helper::getStatusData($data['reputation']['status']),
            "blacklists_entries" => Helper::blacklistsEntries(
              $data['reputation']['status'],
              $data['reputation']['virusList']),
            "info" => Helper::getReputationInfo($data['reputation']['status']),
            "last_test" => Helper::dateFormatter($data['reputation']['lastTest']['time']),
          ],

          '#attached' => [
            'library' => [
              'wtotem/wtotem.dashboard',
            ],
          ],
          '#theme' => 'wtotem_monitoring',
        ];

        // Scanning blocks.

        $build['scanning'] = [
          "#ports"  => [
            'status' => Helper::getStatusData($data['ports']['status']),
            "TCPResults" => Helper::getOpenPortsData($data['ports']['TCPResults']),
            "ignore_ports" => $data['ports']['ignorePorts'],
            "last_test" => Helper::dateFormatter($data['ports']['lastTest']['time']),
          ],
          "#open_path" => [
            'status' => Helper::getStatusData(($data['openPathSearch']['paths']) ? 'warning' : 'clean'),
            "last_test" => Helper::dateFormatter($data['openPathSearch']['createdAt']),
            "paths" => $data['openPathSearch']['paths'],
          ],

          '#attached' => [
            'library' => [
              'wtotem/wtotem.dashboard',
            ],
          ],
          '#theme' => 'wtotem_scanning',
        ];

        // Commands Ajax.
        $ajax_response->addCommand(new ReplaceCommand('#scoring', $build['scoring']));
        $ajax_response->addCommand(new ReplaceCommand('#firewall_stats', $build['firewall_stats']));
        $ajax_response->addCommand(new ReplaceCommand('#waf_chart_btn', $build['chart_periods']));
        $ajax_response->addCommand(new ReplaceCommand('#firewall_data', $build['firewall_data']));
        $ajax_response->addCommand(new ReplaceCommand('#server_status_cpu', $build['server_status_cpu']));
        $ajax_response->addCommand(new ReplaceCommand('#server_status_ram', $build['server_status_ram']));
        $ajax_response->addCommand(new ReplaceCommand('#antivirus_stats', $build['antivirus_stats']));
        $ajax_response->addCommand(new ReplaceCommand('#monitoring', $build['monitoring']));
        $ajax_response->addCommand(new ReplaceCommand('#scanning', $build['scanning']));

        break;
    }

    self::notifications($ajax_response);

    // Return response.
    return $ajax_response;
  }

  /**
   * Notification output.
   *
   * @param mixed $ajax_response
   *   AjaxResponse.
   */
  public static function notifications(&$ajax_response) {

    $notifications = Helper::getNotifications();

    $build[] = [
      "#notifications" => $notifications,
      '#attached' => [
        'library' => [
          'wtotem/wtotem.notifications',
        ],
      ],
      '#theme' => 'wtotem_notifications',
    ];

    // Commands Ajax.
    $ajax_response->addCommand(new AppendCommand('#wtotem_notifications', $build));
  }

}
