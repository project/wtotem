CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

 WebTotem module provides Drupal that prevents hacking and attacks on a website. 

 * For a full description:
   https://drupal.org/project/wtotem

 * Issue queue for WebTotem:
   https://drupal.org/project/issues/wtotem

REQUIREMENTS
------------

No requirements.

INSTALLATION
------------

 * Install normally as other modules are installed. For Support:
   https://www.drupal.org/docs/extending-drupal/installing-modules


CONFIGURATION
-------------

 * Configure WebTotem in Administration >> Configuration >> System.


MAINTAINERS
-------------

 * Yerlan Zhanakov (Yeonik) - https://www.drupal.org/u/yeonik
