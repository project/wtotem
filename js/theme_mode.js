let theme_mode_ajax = new Drupal.Ajax(false, false, {
    url: Drupal.url('wtotem/change_theme_mode')
});

const theme_mode = document.getElementById('theme_mode');
theme_mode.addEventListener('click', function(){theme_mode_ajax.execute()}, false );
