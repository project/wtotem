let ajax_agents = new Drupal.Ajax(false, false, {
  url: Drupal.url('wtotem/agents_installation_progress')
});

/* run the check every 10 seconds */
let wtotem_agents_status = setInterval(() => ajax_agents.execute(), 10000);

/* stop check after 30 min */
setTimeout(() => {
  clearInterval(wtotem_agents_status);
}, 1800000);