jQuery(document).ready(function ($) {
  $('#wtotem-dos').on('change', function () {
    let checked = $(this).prop('checked');
    toggleLimits($('#wtotem-dos-limit'), $('#wtotem-dos-limit input'), checked);
  });

  $('#wtotem-login-attempts').on('change', function () {
    let checked = $(this).prop('checked');
    toggleLimits($('#wtotem-login-attempts-limit'), $('#wtotem-login-attempts-limit input'), checked);
  });

  function toggleLimits(limitWrap, limitInput, checked) {
    (checked) ? limitWrap.removeClass('visually-hidden') : limitWrap.addClass('visually-hidden');
    (checked) ? limitInput.removeAttr('disabled') : limitInput.attr('disabled', 'disabled');
  }

  $('ul.wtotem-tabs__caption').on('click', 'li:not(.active)', function () {
    $(this)
      .addClass('active').siblings().removeClass('active')
      .closest('div.wtotem-tabs').find('div.wtotem-tabs__content').removeClass('active').eq($(this).index()).addClass('active');
  });

  let show_port_details = (item) => {
    jQuery('.port-cve__technology-container').show();
    jQuery('.port__result-list-item').removeClass('port__result-list-item--selected');

    $("#port_result_list").find("[data-port="+item.data('port')+"]").addClass('port__result-list-item--selected');

    jQuery('#add_port').attr('href', '/wtotem/ignore_ports/add/' + item.data('port'));
    jQuery('#technology_name').html(item.data('technology'));
    jQuery('#technology_version').html(item.data('version'));
    jQuery('#cve_container').html(item.data('summary'));

  };

  $('body').on('click', '#wtotem_ps_settings, .port-scanner-ports .port__result-list-item', function () {
    $('#port-scanner-popup').removeClass('d-none');
  }).on('click', '.port-scanner-list__header--close', function (e) {
    $('#port-scanner-popup').addClass('d-none');
  }).on('click', '.popup-overlay', function (e) {
    if (e.target.className.includes('popup-overlay')) {
      $('.popup-overlay').addClass('d-none');
      $('body').removeClass('lock');
    }
  }).on('click', '.firewall-configuration__multi-adding', function () {
    $('#wtotem-ip-list-type').val($(this).attr('data-list'));
  }).on('click', '.wtotem_reports-accordion__title', function () {
    $(this).next('div').toggleClass('visually-hidden')
  }).on('click', '.wtotem_alert__close', function () {
    $(this).parent('.wtotem_alert').remove();
  }).on('click', '.port__result-list-item', function (e) {
    show_port_details(jQuery(this));
  });

  $('.wtotem_body').on('click', '.wtotem_firewall-log__show-more', function (e) {
    let data_info = $.parseJSON($(this).attr('data-more'));
    $('#wt_user_ip').text(data_info.ip);
    $('#wt_proxy_ip').text(data_info.proxy_ip);
    $('#wt_source').text(data_info.source);
    $('#wt_request').text(data_info.request);
    $('#wt_user_agent').text(data_info.user_agent);
    $('#wt_time').text(data_info.time);
    $('#wt_type').text(data_info.type);
    $('#wt_category').text(data_info.category);
    $('#wt_country').text(data_info.country);
    $('#wt_payload').text(data_info.payload);
    $('#firewall-log-report').removeClass('d-none');
  });


});


// Hamburger menu.
const burgerEl = document.querySelector(".wtotem_burger");
const menuEl = document.querySelector(".wtotem_nav");
const bodyEl = document.getElementsByTagName("body")[0];

if (burgerEl) {
  burgerEl.addEventListener("click", () => {
    burgerEl.classList.toggle("active");
    menuEl.classList.toggle("active");
    // Adds body overflow hidden when the menu is open.
    bodyEl.classList.toggle("lock");
  });
}

// Report generate Modal.
const reportsBtn = document.querySelectorAll(".add_report");
const reportsModal = document.querySelector(".wtotem_reports-modal");
const reportsModalClose = document.querySelector(".wtotem_reports-modal__close");
const reportsModalMessage = document.querySelector("#wtotem_reports_form-messages");

if (reportsBtn) {
  for (let elem of reportsBtn) {
    elem.addEventListener("click", () => {
      reportsModal.classList.add("wtotem_reports-modal--active");
      bodyEl.classList.add("lock");
    });
  }
}

if (reportsModalClose) {
  reportsModalClose.addEventListener("click", () => {
    reportsModal.classList.remove("wtotem_reports-modal--active");
    bodyEl.classList.remove("lock");
    reportsModalMessage.innerHTML = '';
  });
}


const addSideModal = (modal, openButton, closeButton, form) => {

  const overlay = document.querySelector(".side-modal__overlay");
  const message = document.querySelector(".wtotem_input__success");
  const bodyEl = document.getElementsByTagName("body")[0];
  const toggleClassName = "side-modal--opened";
  const toggleBody = "lock";

  const showModal = () => {
    modal.classList.add(toggleClassName);
    bodyEl.classList.add(toggleBody);
  };

  const closeModal = () => {
    modal.classList.remove(toggleClassName);
    bodyEl.classList.remove(toggleBody);
    if (form) {
      form.reset();
      message.innerHTML = '';
    }
  };

  openButton.addEventListener("click", showModal);
  closeButton.addEventListener("click", closeModal);
  overlay.addEventListener("click", closeModal);
};

(function () {
  const modal = document.querySelector(".firewall-multi-adding");
  const openButton = document.querySelector(".firewall-configuration__multi-adding");
  const openButton2 = document.querySelector(".multi-adding-deny_list");
  const closeButton = document.querySelector(".firewall-multi-adding__close");
  const form = document.querySelector(".wtotem-allow-deny-multi-add-form");

  if (modal) {
    addSideModal(modal, openButton, closeButton, form);
    addSideModal(modal, openButton2, closeButton, form);
  }
})();

(function () {
  const modal = document.querySelector(".country-blocking-modal");
  const openButton = document.querySelector("#block_countries_btn");
  const closeButton = document.querySelector(".country-blocking-modal__closeBtn");

  if (modal) {
    addSideModal(modal, openButton, closeButton, false);
  }
})();

window.addEventListener('DOMContentLoaded', function () {

  function tlite(t) {
    document.addEventListener("mouseover", function (e) {
      var i = e.target, n = t(i);
      n || (n = (i = i.parentElement) && t(i)), n && tlite.show(i, n, !0)
    })
  }

  tlite.show = function (t, e, i) {
    var n = "data-tlite";
    e = e || {}, (t.tooltip || function (t, e) {
      function o() {
        tlite.hide(t, !0)
      }

      function l() {
        r || (r = function (t, e, i) {
          function n() {
            o.className = "tlite tlite-" + r + s;
            var e = t.offsetTop, i = t.offsetLeft;
            o.offsetParent === t && (e = i = 0);
            var n = t.offsetWidth, l = t.offsetHeight, d = o.offsetHeight, f = o.offsetWidth, a = i + n / 2;
            o.style.top = ("s" === r ? e - d - 10 : "n" === r ? e + l + 10 : e + l / 2 - d / 2) + "px", o.style.left = ("w" === s ? i : "e" === s ? i + n - f : "w" === r ? i + n + 10 : "e" === r ? i - f - 10 : a - f / 2) + "px"
          }

          var o = document.createElement("span"), l = i.grav || t.getAttribute("data-tlite") || "n";
          o.innerHTML = e, t.appendChild(o);
          var r = l[0] || "", s = l[1] || "";
          n();
          var d = o.getBoundingClientRect();
          return "s" === r && d.top < 0 ? (r = "n", n()) : "n" === r && d.bottom > window.innerHeight ? (r = "s", n()) : "e" === r && d.left < 0 ? (r = "w", n()) : "w" === r && d.right > window.innerWidth && (r = "e", n()), o.className += " tlite-visible", o
        }(t, d, e))
      }

      var r, s, d;
      return t.addEventListener("mousedown", o), t.addEventListener("mouseleave", o), t.tooltip = {
        show: function () {
          d = t.title || t.getAttribute(n) || d, t.title = "", t.setAttribute(n, ""), d && !s && (s = setTimeout(l, i ? 150 : 1))
        }, hide: function (t) {
          if (i === t) {
            s = clearTimeout(s);
            var e = r && r.parentNode;
            e && e.removeChild(r), r = void 0
          }
        }
      }
    }(t, e)).show()
  }, tlite.hide = function (t, e) {
    t.tooltip && t.tooltip.hide(e)
  }, "undefined" != typeof module && module.exports && (module.exports = tlite);

  // init tooltip with class js--tooltip
  tlite(el => {
    return {
      el: el.classList.contains('js--tooltip'), grav: 'sw',
    }
  });

});
/**
 * Calendar init
 *
 * @param element
 *   Input selector element with the start date of the period.
 * @param dateFromSelector
 *   Input selector with the start date of the period.
 * @param dateToSelector
 *   Input selector with the end date of the period.
 * @param toggler
 *    Selector element of the wrapper of the elements that open the calendar when clicked.
 * @returns {*}
 * @private
 */
const setFlatpickr_ = (element, dateFromSelector, dateToSelector, toggler) => {
  const calendar = flatpickr(element,
    {
      mode: "range",
      dateFormat: "j M, Y",
      onChange: function (selectedDates) {
        const dates = selectedDates.map(date => this.formatDate(date, "j M, Y"));
        document.querySelector(dateFromSelector).value = dates[0];
        if (dates[1]) {
          document.querySelector(dateToSelector).value = dates[1];
        }
      }
    });

  toggler.addEventListener("click", calendar.open);

  return calendar;
};
