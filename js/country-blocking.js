const countrySearchForm = document.querySelector('#country-search');
const searchInput = countrySearchForm.querySelector('input');
const countryBlockingForm = document.querySelector('#country-blocking');
const regionsContainer = countryBlockingForm.querySelector(
  '.country-blocking-form__main'
);
const selectAll = document.querySelector('#select-all');
const regionButtons = document.querySelectorAll('.country-blocking-accordion');

const countryBlockingFormMain = document.querySelector('.country-blocking-form__main');

const allCountryNames = Array.from(
  countryBlockingFormMain.querySelectorAll('.form-item__label')
).map((node) => node.querySelector('p'));

const countryContainers = document.querySelectorAll(
  '.country-blocking-form__region-countries'
);

countryContainers.forEach((container, index) => {
  const numNodes = document.querySelectorAll('.blocked-counter');
  container.addEventListener('change', updateCounter(numNodes[index]));
});
selectAll.addEventListener('change', (event) => {
  const allCountries = regionsContainer.querySelectorAll('[type=checkbox]');
  allCountries.forEach((checkbox) => {
    checkbox.checked = event.target.checked;
    checkbox.dataset.checked = event.target.checked;
  });

  const numNodes = document.querySelectorAll('.blocked-counter');
  const countriesContainers = document.querySelectorAll(
    '.country-blocking-form__region-countries'
  );
  numNodes.forEach((node, index) => {
    node.textContent = event.target.checked
      ? countriesContainers[index].querySelectorAll('.country-checkbox').length
      : 0;
  });

});
const selectAllRegionCheckboxes =
  document.querySelectorAll('.select-all-region');

selectAllRegionCheckboxes.forEach((checkbox) =>
  checkbox.addEventListener('change', toggleSelectRegion)
);

regionButtons.forEach((btn) => btn.addEventListener('click', toggleOpen));

function toggleOpen() {
  const countriesContainer = this.nextElementSibling;
  const svg = this.querySelector('svg');
  countriesContainer.classList.toggle('country-blocking-accordion--open');
  countriesContainer.classList.toggle(
    'country-blocking-form__region-countries--open'
  );
  svg.classList.toggle('chevron--open');
}

function toggleSelectRegion(event) {
  const regionCheckboxes =
    this.parentElement.parentElement.querySelectorAll('.country-checkbox');
  const {checked} = event.target;
  regionCheckboxes.forEach((checkbox) => {
    checkbox.checked = checked;
    checkbox.dataset.checked = checked;
    if (checked) checkedCountries.push(checkbox.name);
    else
      checkedCountries = checkedCountries.filter(
        (country) => country !== checkbox.name
      );
  });
}

let checkedCountries = [];

function updateCounter(node) {
  return (event) => {
    const {checked} = event.target;

    if (event.target.classList.contains('select-all-region')) {
      const allRegion =
        event.target.parentElement.parentElement.querySelectorAll(
          '.country-checkbox'
        );
      return checked
        ? (node.textContent = allRegion.length)
        : (node.textContent = 0);
    }

    const num = Number(node.textContent);
    node.textContent = checked ? num + 1 : num > 0 ? num - 1 : 0;
    event.target.dataset.checked = checked;
    if (!checked) {
      checkedCountries = checkedCountries.filter(
        (country) => country !== event.target.name
      );
    } else {
      checkedCountries.push(event.target.name);
    }
    const allRegionCheckbox =
      event.target.parentElement.parentElement.parentElement.parentElement.parentElement.querySelector(
        '.select-all-region'
      );
    const allRegionCheckboxes = Array.from(
      event.target.parentElement.parentElement.querySelectorAll(
        '.country-checkbox'
      )
    );
    allRegionCheckboxes.splice(
      allRegionCheckboxes.findIndex(
        (checkbox) => checkbox.name === event.target.name
      ),
      1,
      event.target
    );

    const isRegionChecked = allRegionCheckboxes.every((checkbox) =>
      JSON.parse(checkbox.dataset.checked || false)
    );

    if (isRegionChecked) {
      allRegionCheckbox.checked = true;
      allRegionCheckbox.dataset.checked = true;
    } else {
      allRegionCheckbox.checked = false;
      allRegionCheckbox.dataset.checked = false;
    }
  };
}

let timeout;

searchInput.addEventListener('input', (event) => {
  console.log(1);
  clearTimeout(timeout);
  timeout = setTimeout(() => {
    const query = event.target.value.trim().toLowerCase();
    const foundCountries = allCountryNames.filter((node) =>
      node.textContent.toLowerCase().includes(query.toLocaleLowerCase())
    );
    const countriesToShow = foundCountries.map(
      (node) => node.parentElement.parentElement
    );
    allCountryNames.forEach((node) =>
      node.parentElement.parentElement.classList.add('hidden')
    );
    countriesToShow.forEach((node) => node.classList.remove('hidden'));
    const allRegions = document.querySelectorAll(
      '.country-blocking-form__region-countries'
    );
    allRegions.forEach((region) => {
      const regionCountries = Array.from(
        region.querySelector('fieldset').querySelectorAll('.js-form-item')
      );
      const isRegionToHide = regionCountries.every((country) =>
        country.classList.contains('hidden')
      );
      if (isRegionToHide) {
        region.classList.add('hidden');
        region.previousElementSibling.classList.add('hidden');
      } else {
        region.classList.remove('hidden');
        region.previousElementSibling.classList.remove('hidden');
      }
    });
  }, 500);
});
