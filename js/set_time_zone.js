let visitorTime = new Date();
let time_zone_offset = -visitorTime.getTimezoneOffset() / 60;

let ajax_user_time_zone = new Drupal.Ajax(false, false, {
    url: Drupal.url('wtotem/user_time_zone/' + time_zone_offset)
});

ajax_user_time_zone.execute();
