let ajax_reload_page = new Drupal.Ajax(false, false, {
  url: Drupal.url('wtotem/reload_page/dashboard'),
});

/* run the reload every 60 seconds */
let wtotem_reload_page = setInterval(() => ajax_reload_page.execute(), 60000);

/* stop reload after 30 min */
setTimeout(() => {
  clearInterval(wtotem_reload_page);
}, 1800000);