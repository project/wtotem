<?php

/**
 * @file
 * Allows administrators to improve security of the website.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Defining constants.
 */
if (!defined('WTOTEM_MODULE_PATH')) {
  define('WTOTEM_MODULE_PATH', '/' . \Drupal::service('extension.list.module')->getPath('wtotem'));
}

if (!defined('WTOTEM_ROOT_PATH')) {
  define('WTOTEM_ROOT_PATH', DRUPAL_ROOT . WTOTEM_MODULE_PATH . '/');
}

if (!defined('WTOTEM_VERSION')) {
  define('WTOTEM_VERSION', '1.2.1');
}

function wtotem_requirements($phase) {
	$requirements = array();
	if ($phase == 'install') {
		$requirements['module_version'] = array(
			'title' => t('Module version'),
			'value' => WTOTEM_VERSION,
			'severity' => REQUIREMENT_INFO
		);
	}
	return $requirements;
}

/**
 * Implements hook_help().
 */
function wtotem_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.wtotem':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The WebTotem Security module for Drupal monitors websites and prevents website attacks with the help of special internal and external utilities. For an extensive listing of these utilities see the <a href=":project">WebTotem Security</a> project site.', [':project' => 'https://www.drupal.org/project/wtotem']) . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dt>' . t('Configuring WebTotem Security') . '</dt>';
      $output .= '<dd>' . t('All settings for this module can be found on the <a href=":wt_settings">WebTotem Security settings</a> page.', [':wt_settings' => Url::fromRoute('wtotem.settings_page')->toString()]) . '</dd>';
      $output .= '<dt>' . t('Activation of the WebTotem Security') . '</dt>';
      $output .= '<dd>' . t('Activation of this module can be found on the <a href=":wt_activation">activation</a> page.', [':wt_activation' => Url::fromRoute('wtotem.settings')->toString()]) . '</dd>';
  }

  return $output;
}

/**
 * Implements hook_theme().
 */
function wtotem_theme($existing, $type, $theme, $path) {
  return [

    'wtotem_header' => [
      'variables' => [
        'notifications' => [],
        'theme_mode' => [],
        'is_active' => [],
        'images_path' => NULL,
        'created_at' => date("Y-m-d H:i:s"),
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_footer' => [
      'variables' => [
		    'images_path' => NULL,
		    'current_year' => date('Y'),
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_popup' => [
      'variables' => [
        'message' => NULL,
        'action' => NULL,
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_notifications' => [
      'variables' => [
        'notifications' => [],
        'images_path' => NULL,
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_section_header' => [
      'variables' => [
        'title' => NULL,
        'tooltip' => [],
      ],
    ],

    'wtotem_chart_periods' => [
      'variables' => [
        'service' => NULL,
        'days' => 7,
      ],
      'path' => WTOTEM_MODULE_PATH . '/templates/parts',
    ],

    'wtotem_agents' => [
      'variables' => [
        'images_path' => NULL,
        'process_status' => [],
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_agents_installation' => [
      'variables' => [
        'images_path' => NULL,
        'process_status' => [],
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_score' => [
      'variables' => [
        'total_score' => NULL,
        'tested_on' => NULL,
        'server_ip' => NULL,
        'location' => NULL,
        'is_higher_than' => NULL,
        'grade' => NULL,
        'color' => NULL,
        'host_id' => NULL,
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_firewall_header' => [
      'variables' => [
        'title' => NULL,
        'tooltip' => [],
        'settings' => [],
        'images_path' => NULL,
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_firewall' => [
      'variables' => [
        'chart' => NULL,
        'days' => 7,
        'logs' => NULL,
        'page' => NULL,
        'has_next_page' => NULL,
	    'images_path' => NULL,
      ],
    ],

    'wtotem_firewall_stats' => [
      'variables' => [
        'is_waf_training' => NULL,
        'all_attacks' => NULL,
        'blocking' => NULL,
        'not_blocking' => NULL,
        'most_attacks' => NULL,
      ],
    ],

    'wtotem_firewall_chart' => [
      'variables' => [
        'chart' => NULL,
        'days' => 7,
      ],
    ],

    'wtotem_firewall_logs' => [
      'variables' => [
        'logs' => [],
        'has_next_page' => NULL,
      ],
    ],

    'wtotem_antivirus' => [
      'variables' => [
        'logs' => [],
        'has_next_page' => NULL,
        'last_scan' => NULL,
        'images_path' => NULL,
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_antivirus_logs' => [
      'variables' => [
        'logs' => [],
        'has_next_page' => NULL,
        'images_path' => NULL,
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_quarantine' => [
      'variables' => [
        'logs' => [],
        'count' => NULL,
      ],
    ],

    'wtotem_quarantine_logs' => [
      'variables' => [
        'logs' => [],
      ],
    ],

    'wtotem_attacks_map' => [
      'variables' => [
        'world_map_json' => NULL,
        'attacks_map' => NULL,
        'days' => 7,
      ],
    ],

    'wtotem_map_chart' => [
      'variables' => [
        'world_map_json' => NULL,
        'attacks_map' => NULL,
      ],
    ],

    'wtotem_server_status_ram' => [
      'variables' => [
        'info' => [],
        'days' => 7,
        'ram_chart' => NULL,
      ],
    ],

    'wtotem_server_status_cpu' => [
      'variables' => [
        'days' => 7,
        'cpu_chart' => NULL,
      ],
    ],

    'wtotem_cpu_chart' => [
      'variables' => [
        'chart' => NULL,
        'days' => 7,
      ],
    ],

    'wtotem_ram_chart' => [
      'variables' => [
        'chart' => NULL,
        'days' => 7,
      ],
    ],

    'wtotem_antivirus_stats' => [
      'variables' => [
        'changes' => NULL,
        'scanned' => NULL,
        'deleted' => NULL,
        'infected' => NULL,
        'page' => NULL,
      ],
    ],

    'wtotem_monitoring' => [
      'variables' => [
        'ssl' => [],
        'domain' => [],
        'reputation' => [],
      ],
    ],

    'wtotem_scanning' => [
      'variables' => [
        'ports' => [],
        'open_path' => [],
        'images_path' => NULL,
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_ignore_ports' => [
      'variables' => [
        'ports' => [],
        'images_path' => NULL,
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_open_ports' => [
      'variables' => [
        'ports' => [],
        "more" => false,
        'images_path' => NULL,
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_open_paths' => [
      'variables' => [
        'paths' => [],
        'images_path' => NULL,
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_open_paths_page' => [
      'variables' => [
        'paths' => [],
        'images_path' => NULL,
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_reports' => [
      'variables' => [
        'reports' => [],
        'has_next_page' => NULL,
      ],
    ],

    'wtotem_reports_list' => [
      'variables' => [
        'reports' => [],
        'has_next_page' => NULL,
      ],
    ],

    'wtotem_reports_list_mobile' => [
      'variables' => [
        'reports' => [],
        'has_next_page' => NULL,
      ],
    ],

    'wtotem_allow_deny_list' => [
      'variables' => [
        'list' => [],
        'images_path' => NULL,
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_allow_url_list' => [
      'variables' => [
        'list' => [],
        'images_path' => NULL,
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_country_blocking_modal' => [
        'variables' => [
            'blocked_countries_list' => '',
            'mock_attacks' => '',
            'images_path' => NULL,
        ],
        'file' => 'wtotem.theme.inc',
    ],

    'wtotem_information' => [
      'variables' => [],
    ],

    'wtotem_authorization_needed' => [
      'variables' => [
        'images_path' => NULL,
      ],
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_settings_form' => [
      'render element' => 'form',
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_login_form' => [
      'render element' => 'form',
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_ports_form' => [
      'render element' => 'form',
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_reports_form' => [
      'render element' => 'form',
    ],

    'wtotem_port_scanner_form' => [
      'render element' => 'form',
    ],

    'wtotem_waf_filter_form' => [
      'render element' => 'form',
      'file' => 'wtotem.theme.inc',
    ],

    'wtotem_antivirus_filter_form' => [
      'render element' => 'form',
      'file' => 'wtotem.theme.inc',
    ],

  ];
}
